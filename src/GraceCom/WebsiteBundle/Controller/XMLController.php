<?php

namespace GraceCom\WebsiteBundle\Controller;

use Doctrine\ORM\Mapping\OrderBy;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/feeds")
 */
class XMLController extends Controller
{
    
	/**
	* @Route("/audiofiles.xml", name="_xml_audiofiles")
    * @Template("GraceWebBundle:XML:audioFiles.xml.twig")
	*/
    public function audiofilesAction()
    {
    	$audiofiles = $this->getDoctrine()->getRepository('GraceWebBundle:AudioFile')->findAll();
    	if (!$audiofiles) $this->createNotFoundException('No audiofiles found');
    	return array('audiofiles'=>$audiofiles);
    }
}
