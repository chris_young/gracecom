<?php

namespace GraceCom\WebsiteBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;

use GraceCom\WebsiteBundle\GraceWebBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

use GraceCom\WebsiteBundle\Form\PersonType;
use GraceCom\WebsiteBundle\Form\ProfileType;
use GraceCom\WebsiteBundle\Form\CongregationType;
use GraceCom\WebsiteBundle\Form\ServiceType;
use GraceCom\WebsiteBundle\Form\EventType;
use GraceCom\WebsiteBundle\Form\ArticleType;
use GraceCom\WebsiteBundle\Form\AudioFileType;
use GraceCom\WebsiteBundle\Form\ResourceCategoryType;
use GraceCom\WebsiteBundle\Form\ResourceFileType;
use GraceCom\WebsiteBundle\Form\MinistryType;
use GraceCom\WebsiteBundle\Form\CellType;
use GraceCom\WebsiteBundle\Form\UserType;
use GraceCom\WebsiteBundle\Form\RosterroleType;
use GraceCom\WebsiteBundle\Form\RosterLineType;
use GraceCom\WebsiteBundle\Form\RosterItemType;
use GraceCom\WebsiteBundle\Form\RosterItemPersonType;
use GraceCom\WebsiteBundle\Form\RosterItemTextType;
use GraceCom\WebsiteBundle\Form\PageType;
use GraceCom\WebsiteBundle\Form\FaultType;
use GraceCom\WebsiteBundle\Form\FeatureType;
use GraceCom\WebsiteBundle\Form\FaultCommentType;
use GraceCom\WebsiteBundle\Form\FeatureCommentType;
use GraceCom\WebsiteBundle\Entity\Person;
use GraceCom\WebsiteBundle\Entity\Profile;
use GraceCom\WebsiteBundle\Entity\Congregation;
use GraceCom\WebsiteBundle\Entity\Service;
use GraceCom\WebsiteBundle\Entity\Event;
use GraceCom\WebsiteBundle\Entity\Article;
use GraceCom\WebsiteBundle\Entity\User;
use GraceCom\WebsiteBundle\Entity\AudioFile;
use GraceCom\WebsiteBundle\Entity\ResourceCategory;
use GraceCom\WebsiteBundle\Entity\ResourceFile;
use GraceCom\WebsiteBundle\Entity\Ministry;
use GraceCom\WebsiteBundle\Entity\Cell;
use GraceCom\WebsiteBundle\Entity\Rosterrole;
use GraceCom\WebsiteBundle\Entity\RosterLine;
use GraceCom\WebsiteBundle\Entity\RosterItem;
use GraceCom\WebsiteBundle\Entity\Page;
use GraceCom\WebsiteBundle\Entity\Fault;
use GraceCom\WebsiteBundle\Entity\FaultComment;
use GraceCom\WebsiteBundle\Entity\Feature;
use GraceCom\WebsiteBundle\Entity\FeatureComment;

/**
 * @Route("/secured")
 */
class SecuredController extends Controller
{
	/**
	* @Route("/", name="_admin_index")
     * @Secure(roles="ROLE_ADMIN")
	* @Template()
	*/
    public function indexAction()
    {
    	return array('content'=>'');
    }
    
    
    /**
     * @Route("/login", name="_grace_login")
     * @Template()
     */
    public function loginAction()
    {
        if ($this->get('request')->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $this->get('request')->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $this->get('request')->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        return array(
            'last_username' => $this->get('request')->getSession()->get(SecurityContext::LAST_USERNAME),
            'error'         => $error,
        );
    }

    /**
     * @Route("/login_check", name="_security_check")
     */
    public function securityCheckAction()
    {
        // The security layer will intercept this request
    }

    /**
     * @Route("/logout", name="_logout")
     */
    public function logoutAction()
    {
        // The security layer will intercept this request
    }
    
    /**
    * @Route("/people", name="_admin_people")
     * @Secure(roles="ROLE_ADMIN")
    * @Template()
    */
    public function peopleAction()
    {     
    	$query = $this->getDoctrine()->getEntityManager()->createQueryBuilder();
    	$query->select('p')->add('from','GraceWebBundle:Person p')
    			->add('orderBy','p.last_name','ASC')
    			->add('orderBy','p.first_name','ASC');
    	$request = $this->get('request');
    	$filter = array('congregation'=>'');
    	if ('POST' == $request->getMethod()) {
    		$filter = array(); $parameters = array();
    		$filter['congregation'] = $request->get('congregation');
    		if ($filter['congregation'] == 'none') {
    			$query->andWhere('p.congregation is null');
    		}
    		else if ($filter['congregation']) {
    			$query->andWhere('p.congregation = :congregation');
    			$query->setParameter('congregation',$filter['congregation']);
    		}
    	}
    	$people = $query->getQuery()->getResult();
    	
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder()
    	->select('c')->add('from','GraceWebBundle:Congregation c')
    	->where('c.published = 1')->orderBy('c.id','ASC');
    	$congregations = $qb->getQuery()->getResult();
    	
    	if (!$people) $this->createNotFoundException('No people were found');
    	return array('people'=>$people,'congregations'=>$congregations,'filter'=>$filter);
    }
    
    /**
     * @Route("/editperson/{id}", name="_edit_person")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formpersonedit.html.twig")
     */
    public function editpersonAction($id)
    {
    	$person = $this->getDoctrine()->getRepository('GraceWebBundle:Person')->find($id);
    	if (!$person) $this->createNotFoundException('Person not found');
    	$form = $this->get('form.factory')->create(new PersonType(),$person);
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$person = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($person);
    			$em->flush();
    			 
    			$mailer = $this->get('mailer');
    			// .. setup a message and send it
    			// http://symfony.com/doc/current/cookbook/email.html
    	
    			$this->get('session')->setFlash('notice', 'Person Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_people'));
    		}
    	}
    	
    	 
    	return array('person'=>$person,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deleteperson/{id}", name="_delete_person")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formpersondelete.html.twig")
     */
    public function deletepersonAction($id)
    {
    	$person = $this->getDoctrine()->getRepository('GraceWebBundle:Person')->find($id);
    	if (!$person) $this->createNotFoundException('person not found');
    	$form = $this->get('form.factory')->create(new PersonType(),$person);
    	
    	$request = $this->get('request');
    	
    	if ('POST' == $request->getMethod())
    	{
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($person);
    		$em->flush();
    		
    		$this->get('session')->setFlash('notice', 'Person Deleted!');
    		
    		return $this->redirect($this->generateUrl('_admin_people'));
    	}
    	
    	return array('form'=>$form->createView(),'person'=>$person);
    }
    /**
     * @Route("/addperson", name="_add_person")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formpersonadd.html.twig")
     */
    public function addpersonAction()
    {
    	$person = new Person();
    	$form = $this->get('form.factory')->create(new PersonType($person));
    	    	
    	$request = $this->get('request');
        if ('POST' == $request->getMethod()) {
            $form->bindRequest($request);
            if ($form->isValid()) {
				$person = $form->getData();
            	$em = $this->getDoctrine()->getEntityManager();
            	$em->persist($person);
            	$em->flush();
            	
            	$mailer = $this->get('mailer');
                // .. setup a message and send it
                // http://symfony.com/doc/current/cookbook/email.html

                $this->get('session')->setFlash('notice', 'Person Saved!');

                return $this->redirect($this->generateUrl('_admin_people'));
            }
        }

        return array('form' => $form->createView());
    }
    
    /**
    * @Route("/profiles", name="_admin_profiles")
    * @Secure(roles="ROLE_ADMIN")
    * @Template("")
    */
    public function profilesAction()
    {
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('p')->add('from','GraceWebBundle:Profile p')->orderBy('p.list_order','ASC');
    	$profiles = $qb->getQuery()->getResult();
    	if (!$profiles) $this->createNotFoundException('No profiles were found');
    	return array('profiles'=>$profiles);
    }
    
    /**
     * @Route("/addprofile", name="_add_profile")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formprofileadd.html.twig")
     */
    public function addProfilesAction()
    {
    	$profile = new Profile();
    	$form = $this->get('form.factory')->create(new ProfileType($profile));
    
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$profile = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
            	
            	$qb = $em->createQueryBuilder();
            	$qr = $qb->add('select',$qb->expr()->max('p.list_order'))
            	->add('from','GraceWebBundle:Profile p')
            	->setMaxResults(1)->getQuery()->getSingleResult();
            	$profile->setListOrder($qr[1] + 1);
            	
    			$em->persist($profile);
    			$em->flush();
    			 
    			$mailer = $this->get('mailer');
    			// .. setup a message and send it
    			// http://symfony.com/doc/current/cookbook/email.html
    
    			$this->get('session')->setFlash('notice', 'Profile Saved!');
    
    			return $this->redirect($this->generateUrl('_admin_profiles'));
    		}
    	}
    
    	return array('form' => $form->createView());
    }
    
    /**
     * @Route("/editprofile/{id}", name="_edit_profile")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formprofileedit.html.twig")
     */
    public function editProfileAction($id)
    {
    	$profile = $this->getDoctrine()->getRepository('GraceWebBundle:Profile')->find($id);
    	if (!$profile) $this->createNotFoundException('Profile not found');
    	$form = $this->get('form.factory')->create(new ProfileType(),$profile);
    	 
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$profile = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
                $profile->preUpload();
                $profile->upload();
    			$em->persist($profile);
    			$em->flush();
    			 
    			$this->get('session')->setFlash('notice', 'Profile Saved!');
    			 
    			return $this->redirect($this->generateUrl('_admin_profiles'));
    		}
    	}
    	 
    
    	return array('profile'=>$profile,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deleteprofile/{id}", name="_delete_profile")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formprofiledelete.html.twig")
     */
    public function deleteProfileAction($id)
    {
    	$profile = $this->getDoctrine()->getRepository('GraceWebBundle:Profile')->find($id);
    	if (!$profile) $this->createNotFoundException('Profile not found');
    	$form = $this->get('form.factory')->create(new ProfileType(),$profile);
    	 
    	$request = $this->get('request');
    	 
    	if ('POST' == $request->getMethod())
    	{
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($profile);
    		$em->flush();
    
    		$this->get('session')->setFlash('notice', 'Profile Deleted!');
    
    		return $this->redirect($this->generateUrl('_admin_profiles'));
    	}
    	 
    	return array('form'=>$form->createView(),'profile'=>$profile);
    }
    
    /**
     * @Route("/congregations", name="_admin_congregations")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("")
     */
    public function congregationsAction()
    {
	    $congregations = $this->getDoctrine()->getRepository('GraceWebBundle:Congregation')->findAll();
	        	if (!$congregations) $this->createNotFoundException('No congregations were found');
	    return array('congregations'=>$congregations);
    }
    
    /**
     * @Route("/addcongregation", name="_add_congregation")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formcongregationadd.html.twig")
     */
    public function addcongregationAction()
    {
    	$congregation = new Congregation();
    	$form = $this->get('form.factory')->create(new congregationType(),$congregation);
    	    	
    	$request = $this->get('request');
        if ('POST' == $request->getMethod()) {
            $form->bindRequest($request);
            if ($form->isValid()) {
				$congregation = $form->getData();
            	$em = $this->getDoctrine()->getEntityManager();
            	$em->persist($congregation);
            	$em->flush();

                $this->get('session')->setFlash('notice', 'congregation Saved!');

                return $this->redirect($this->generateUrl('_admin_congregations'));
            }
        }

        return array('form' => $form->createView());
    }
    
    /**
     * @Route("/editcongregation/{id}", name="_edit_congregation")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formcongregationedit.html.twig")
     */
    public function editcongregationAction($id)
    {
    	$congregation = $this->getDoctrine()->getRepository('GraceWebBundle:Congregation')->find($id);
    	if (!$congregation) $this->createNotFoundException('congregation not found');
    	$form = $this->get('form.factory')->create(new congregationType(),$congregation);
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$congregation = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($congregation);
    			$em->flush();
    			 
    			$mailer = $this->get('mailer');
    			// .. setup a message and send it
    			// http://symfony.com/doc/current/cookbook/email.html
    	
    			$this->get('session')->setFlash('notice', 'congregation Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_congregations'));
    		}
    	}
    	
    	 
    	return array('congregation'=>$congregation,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deletecongregation/{id}", name="_delete_congregation")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formcongregationdelete.html.twig")
     */
    public function deletecongregationAction($id)
    {
    	$congregation = $this->getDoctrine()->getRepository('GraceWebBundle:Congregation')->find($id);
    	if (!$congregation) $this->createNotFoundException('Congregation not found');
    	$form = $this->get('form.factory')->create(new CongregationType(),$congregation);
    	 
    	$request = $this->get('request');
    	 
    	if ('POST' == $request->getMethod())
    	{
    		$em = $this->getDoctrine()->getEntityManager();
    		// Remove associations with Events and Cells (other relationships should be cascade remove)
    		foreach ($congregation->getCells() as $cell) $cell->setCongregation(NULL);
    		foreach ($congregation->getEvents() as $event) $event->setCongregation(NULL);
    		$em->remove($congregation);
    		$em->flush();
    
    		$this->get('session')->setFlash('notice', 'Profile Deleted!');
    
    		return $this->redirect($this->generateUrl('_admin_congregations'));
    	}
    	 
    	return array('form'=>$form->createView(),'congregation'=>$congregation);
    }
    
    /**
     * @Route("/services", name="_admin_services")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("")
     */
    public function servicesAction()
    {
	    $services = $this->getDoctrine()->getRepository('GraceWebBundle:Service')->findAll();
	        	if (!$services) $this->createNotFoundException('No services were found');
	    return array('services'=>$services);
    }
    
    /**
     * @Route("/addservices", name="_add_service")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formserviceadd.html.twig")
     */
    public function addservicesAction()
    {
    	$service = new service();
    	$form = $this->get('form.factory')->create(new serviceType($service));
    	    	
    	$request = $this->get('request');
        if ('POST' == $request->getMethod()) {
            $form->bindRequest($request);
            if ($form->isValid()) {
				$service = $form->getData();
            	$em = $this->getDoctrine()->getEntityManager();
            	$em->persist($service);
            	$em->flush();
            	
            	$mailer = $this->get('mailer');
                // .. setup a message and send it
                // http://symfony.com/doc/current/cookbook/email.html

                $this->get('session')->setFlash('notice', 'service Saved!');

                return $this->redirect($this->generateUrl('_admin_services'));
            }
        }

        return array('form' => $form->createView());
    }
    
    /**
     * @Route("/editservice/{id}", name="_edit_service")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formserviceedit.html.twig")
     */
    public function editserviceAction($id)
    {
    	$service = $this->getDoctrine()->getRepository('GraceWebBundle:Service')->find($id);
    	if (!$service) $this->createNotFoundException('service not found');
    	$form = $this->get('form.factory')->create(new serviceType(),$service);
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$service = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($service);
    			$em->flush();
    			 
    			$mailer = $this->get('mailer');
    			// .. setup a message and send it
    			// http://symfony.com/doc/current/cookbook/email.html
    	
    			$this->get('session')->setFlash('notice', 'service Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_services'));
    		}
    	}
    	
    	 
    	return array('service'=>$service,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deleteservice/{id}", name="_delete_service")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formservicedelete.html.twig")
     */
    public function deleteserviceAction($id)
    {
    	$service = $this->getDoctrine()->getRepository('GraceWebBundle:Service')->find($id);
    	if (!$service) $this->createNotFoundException('Service not found');
    	$form = $this->get('form.factory')->create(new ServiceType(),$service);
    	 
    	$request = $this->get('request');
    	 
    	if ('POST' == $request->getMethod())
    	{
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($service);
    		$em->flush();
    
    		$this->get('session')->setFlash('notice', 'Profile Deleted!');
    
    		return $this->redirect($this->generateUrl('_admin_services'));
    	}
    	 
    	return array('form'=>$form->createView(),'service'=>$service);
    }
    
    /**
     * @Route("/events", name="_admin_events")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("")
     */
    public function eventsAction()
    {
	    $events = $this->getDoctrine()->getRepository('GraceWebBundle:Event')->findAll();
	        	if (!$events) $this->createNotFoundException('No events were found');
	    return array('events'=>$events);
    }
    
    /**
     * @Route("/addevents", name="_add_event")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formeventadd.html.twig")
     */
    public function addeventsAction()
    {
    	$event = new event();
    	$form = $this->get('form.factory')->create(new eventType($event));
    	    	
    	$request = $this->get('request');
        if ('POST' == $request->getMethod()) {
            $form->bindRequest($request);
            if ($form->isValid()) {
				$event = $form->getData();
            	$em = $this->getDoctrine()->getEntityManager();
            	$em->persist($event);
            	$em->flush();
            	
            	$mailer = $this->get('mailer');
                // .. setup a message and send it
                // http://symfony.com/doc/current/cookbook/email.html

                $this->get('session')->setFlash('notice', 'event Saved!');

                return $this->redirect($this->generateUrl('_admin_events'));
            }
        }

        return array('form' => $form->createView());
    }
    
    /**
     * @Route("/editevent/{id}", name="_edit_event")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formeventedit.html.twig")
     */
    public function editeventAction($id)
    {
    	$event = $this->getDoctrine()->getRepository('GraceWebBundle:Event')->find($id);
    	if (!$event) $this->createNotFoundException('event not found');
    	$form = $this->get('form.factory')->create(new eventType(),$event);
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$event = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($event);
    			$em->flush();
    	
    			$this->get('session')->setFlash('notice', 'event Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_events'));
    		}
    	}
    	
    	 
    	return array('event'=>$event,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deleteevent/{id}", name="_delete_event")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formeventdelete.html.twig")
     */
    public function deleteeventAction($id)
    {
    	$event = $this->getDoctrine()->getRepository('GraceWebBundle:Event')->find($id);
    	if (!$event) $this->createNotFoundException('event not found');
    	$form = $this->get('form.factory')->create(new EventType(),$event);
    	
    	$request = $this->get('request');
    	
    	if ('POST' == $request->getMethod())
    	{
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($event);
    		$em->flush();
    		
    		$this->get('session')->setFlash('notice', 'Event Deleted!');
    		
    		return $this->redirect($this->generateUrl('_admin_events'));
    	}
    	
    	return array('form'=>$form->createView(),'event'=>$event);
    }
    
    /**
     * @Route("/faults", name="_admin_faults")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("")
     */
    public function faultsAction()
    {
	    $faults = $this->getDoctrine()->getRepository('GraceWebBundle:Fault')->getList();
	    return array('faults'=>$faults);
    }
    
    /**
     * @Route("/faults/{id}", name="_admin_fault")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function faultAction($id)
    {
    	$fault = $this->getDoctrine()->getRepository('GraceWebBundle:Fault')->find($id);
    	if (!$fault) $this->createNotFoundException('fault not found');
    	
    	/* 
    	 * handle fault comments
    	 */
    	$FaultComment = new FaultComment();
    	$form = $this->get('form.factory')->create(new FaultCommentType(),$FaultComment);
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$FaultComment = $form->getData();
    			$FaultComment->setTimestamp(new \DateTime());
    			$FaultComment->setFault($fault);
    			$FaultComment->setCreator($this->get('security.context')->getToken()->getUser());
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($FaultComment);
    			 
    			// .. setup a message and send it
    			$message = \Swift_Message::newInstance()
    			->setSubject('[Gracecom.net.au fault #'.$fault->getId().']')
    			->setFrom('noreply@gracecom.net.au')
    			->setTo($fault->getCreator()->getEmail())
    			->setBody($this->renderView('GraceWebBundle:Secured:emailfaultcomment.txt.twig', 
    				array('fault'=>$fault,'comment'=>$FaultComment)))
    			;
    			$mailer = $this->get('mailer')->send($message);
    			
    			$message = \Swift_Message::newInstance()
    			->setSubject('[Gracecom.net.au fault #'.$fault->getId().']')
    			->setFrom('noreply@gracecom.net.au')
    			->setTo('webmaster@gracecom.net.au')
    			->setBody($this->renderView('GraceWebBundle:Secured:emailfaultcomment.txt.twig',
    				array('fault'=>$fault,'comment'=>$FaultComment)))
    			;
    			$mailer = $this->get('mailer')->send($message);
    			
    			$em->flush();
    	
    			$this->get('session')->setFlash('notice', 'Fault Comment Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_fault',array('id'=>$fault->getId())));
    		}
    	}
    	
    	return array('fault'=>$fault,'form'=>$form->createView());
    }
    
    /**
     * @Route("/addfault", name="_add_fault")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formfaultadd.html.twig")
     */
    public function addfaultsAction()
    {
    	$fault = new fault();
    	$form = $this->get('form.factory')->create(new faultType($fault));
    	    	
    	$request = $this->get('request');
        if ('POST' == $request->getMethod()) {
            $form->bindRequest($request);
            if ($form->isValid()) {
				$fault = $form->getData();
		    	$fault->setTimestamp(new \DateTime());
		    	$fault->setCreator($this->get('security.context')->getToken()->getUser());
            	$em = $this->getDoctrine()->getEntityManager();
            	$em->persist($fault);
            	
            	$em->flush();
            	
            	// .. setup a message and send it
    			$message = \Swift_Message::newInstance()
    			->setSubject('[Gracecom.net.au fault #'.$fault->getId().']')
    			->setFrom('noreply@gracecom.net.au')
    			->setTo('webmaster@gracecom.net.au')
    			->setBody($this->renderView('GraceWebBundle:Secured:emailfault.txt.twig', 
    				array('fault'=>$fault)));
    			$mailer = $this->get('mailer')->send($message);
    			

                $this->get('session')->setFlash('notice', 'Fault Saved!');

                return $this->redirect($this->generateUrl('_admin_faults'));
            }
        }

        return array('form' => $form->createView());
    }
    
    /**
     * @Route("/editfault/{id}", name="_edit_fault")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formfaultedit.html.twig")
     */
    public function editfaultAction($id)
    {
    	$fault = $this->getDoctrine()->getRepository('GraceWebBundle:Fault')->find($id);
    	if (!$fault) $this->createNotFoundException('fault not found');
    	$status = $fault->getStatus();
    	$form = $this->get('form.factory')->create(new faultType(),$fault);
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$fault = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($fault);
    			$em->flush();
            	
            	if ($status != $fault->getStatus())
            	{
	    			// .. setup a message and send it
	    			$message = \Swift_Message::newInstance()
	    			->setSubject('[Gracecom.net.au fault #'.$fault->getId().']')
	    			->setFrom('noreply@gracecom.net.au')
	    			->setTo($fault->getCreator()->getEmail())
	    			->setBody($this->renderView('GraceWebBundle:Secured:emailfaultstatuschange.txt.twig', 
	    				array('fault'=>$fault,'user'=>$this->get('security.context')->getToken()->getUser())));
	    			$mailer = $this->get('mailer')->send($message);
	    			
	    			// .. setup a message and send it
	    			$message = \Swift_Message::newInstance()
	    			->setSubject('[Gracecom.net.au fault #'.$fault->getId().']')
	    			->setFrom('noreply@gracecom.net.au')
	    			->setTo('webmaster@gracecom.net.au')
	    			->setBody($this->renderView('GraceWebBundle:Secured:emailfaultstatuschange.txt.twig', 
	    				array('fault'=>$fault,'user'=>$this->get('security.context')->getToken()->getUser())));
	    			$mailer = $this->get('mailer')->send($message);
            	}
            	
    			$this->get('session')->setFlash('notice', 'Fault Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_faults'));
    		}
    	}
    	
    	 
    	return array('fault'=>$fault,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deletefault/{id}", name="_delete_fault")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formfaultdelete.html.twig")
     */
    public function deletefaultAction($id)
    {
    	$fault = $this->getDoctrine()->getRepository('GraceWebBundle:Fault')->find($id);
    	if (!$fault) $this->createNotFoundException('fault not found');
    	$form = $this->get('form.factory')->create(new FaultType(),$fault);
    	
    	$request = $this->get('request');
    	
    	if ('POST' == $request->getMethod())
    	{
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($fault);
    		$em->flush();
    		
    		$this->get('session')->setFlash('notice', 'Fault Deleted!');
    		
    		return $this->redirect($this->generateUrl('_admin_faults'));
    	}
    	
    	return array('form'=>$form->createView(),'fault'=>$fault);
    }
    
    /**
     * @Route("/editfaultcomment/{id}", name="_edit_faultcomment")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formfaultcommentedit.html.twig")
     */
    public function editfaultcommentAction($id)
    {
    	$FaultComment = $this->getDoctrine()->getRepository('GraceWebBundle:FaultComment')->find($id);
    	if (!$FaultComment) $this->createNotFoundException('fault not found');
    	$form = $this->get('form.factory')->create(new FaultCommentType(),$FaultComment);
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$FaultComment = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($FaultComment);
    			$em->flush();
    	
    			$this->get('session')->setFlash('notice', 'comment Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_fault',array('id'=>$FaultComment->getFault()->getId())));
    		}
    	}
    	
    	 
    	return array('comment'=>$FaultComment,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deletefaultcomment/{id}", name="_delete_faultcomment")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formfaultcommentdelete.html.twig")
     */
    public function deleteFaultCommentAction($id)
    {
    	$FaultComment = $this->getDoctrine()->getRepository('GraceWebBundle:FaultComment')->find($id);
    	if (!$FaultComment) $this->createNotFoundException('comment not found');
    	$form = $this->get('form.factory')->create(new FaultCommentType(),$FaultComment);
    	
    	$request = $this->get('request');
    	
    	if ('POST' == $request->getMethod())
    	{
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($FaultComment);
    		$em->flush();
    		
    		$this->get('session')->setFlash('notice', 'Comment Deleted!');
    		
    		return $this->redirect($this->generateUrl('_admin_fault',array('id'=>$FaultComment->getFault()->getId())));
    	}
    	
    	return array('form'=>$form->createView(),'comment'=>$FaultComment);
    }
    
    /**
     * @Route("/features", name="_admin_features")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("")
     */
    public function featuresAction()
    {
	    $features = $this->getDoctrine()->getRepository('GraceWebBundle:Feature')->findAll();
	        	if (!$features) $this->createNotFoundException('No features were found');
	    return array('features'=>$features);
    }
    
    /**
     * @Route("/features/{id}", name="_admin_feature")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function featureAction($id)
    {
    	$feature = $this->getDoctrine()->getRepository('GraceWebBundle:Feature')->find($id);
    	if (!$feature) $this->createNotFoundException('feature not found');
    	
    	/*
    	 * handle fault comments
    	*/
    	$FeatureComment = new FeatureComment();
    	$form = $this->get('form.factory')->create(new FeatureCommentType(),$FeatureComment);
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$FeatureComment = $form->getData();
    			$FeatureComment->setTimestamp(new \DateTime());
    			$FeatureComment->setFeature($feature);
    			$FeatureComment->setCreator($this->get('security.context')->getToken()->getUser());
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($FeatureComment);
    	
    			// .. setup a message and send it
    			$message = \Swift_Message::newInstance()
    				->setSubject('[Gracecom.net.au feature #'.$feature->getId().']')
    				->setFrom('noreply@gracecom.net.au')
    				->setTo($feature->getCreator()->getEmail())
    				->setBody($this->renderView('GraceWebBundle:Secured:emailfeaturecomment.txt.twig',
    					array('feature'=>$feature,'comment'=>$FeatureComment)));
    			$mailer = $this->get('mailer')->send($message);
    					 
    			$message = \Swift_Message::newInstance()
    				->setSubject('[Gracecom.net.au feature #'.$feature->getId().']')
    				->setFrom('noreply@gracecom.net.au')
    				->setTo('webmaster@gracecom.net.au')
    				->setBody($this->renderView('GraceWebBundle:Secured:emailfeaturecomment.txt.twig',
    						array('feature'=>$feature,'comment'=>$FeatureComment)));
    			
    			$mailer = $this->get('mailer')->send($message);
    							 
    			$em->flush();
    				 
    			$this->get('session')->setFlash('notice', 'Feature Comment Saved!');
    						 
    			return $this->redirect($this->generateUrl('_admin_feature',array('id'=>$feature->getId())));
    		}
    	}
    	
    	return array('feature'=>$feature,'form'=>$form->createView());
    }
    
    /**
     * @Route("/addfeature", name="_add_feature")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formfeatureadd.html.twig")
     */
    public function addfeaturesAction()
    {
    	$feature = new feature();
    	$form = $this->get('form.factory')->create(new featureType($feature));
    	    	
    	$request = $this->get('request');
        if ('POST' == $request->getMethod()) {
            $form->bindRequest($request);
            if ($form->isValid()) {
				$feature = $form->getData();
		    	$feature->setTimestamp(new \DateTime());
		    	$feature->setCreator($this->get('security.context')->getToken()->getUser());
            	$em = $this->getDoctrine()->getEntityManager();
            	$em->persist($feature);
            	$em->flush();
            	
            	// .. setup a message and send it
    			$message = \Swift_Message::newInstance()
    			->setSubject('[Gracecom.net.au feature #'.$feature->getId().']')
    			->setFrom('noreply@gracecom.net.au')
    			->setTo($feature->getCreator()->getEmail())
    			->setBody($this->renderView('GraceWebBundle:Secured:emailfeaturethanks.txt.twig', 
    				array('feature'=>$feature)))
    			;
    			$mailer = $this->get('mailer')->send($message);
    			
    			$message = \Swift_Message::newInstance()
    			->setSubject('[Gracecom.net.au feature #'.$feature->getId().']')
    			->setFrom('noreply@gracecom.net.au')
    			->setTo('webmaster@gracecom.net.au')
    			->setBody($this->renderView('GraceWebBundle:Secured:emailfeature.txt.twig',
    				array('feature'=>$feature)))
    			;
    			$mailer = $this->get('mailer')->send($message);

                $this->get('session')->setFlash('notice', 'Feature Saved!');

                return $this->redirect($this->generateUrl('_admin_features'));
            }
        }

        return array('form' => $form->createView());
    }
    
    /**
     * @Route("/editfeature/{id}", name="_edit_feature")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formfeatureedit.html.twig")
     */
    public function editfeatureAction($id)
    {
    	$feature = $this->getDoctrine()->getRepository('GraceWebBundle:Feature')->find($id);
    	if (!$feature) $this->createNotFoundException('feature not found');
    	$form = $this->get('form.factory')->create(new featureType(),$feature);
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$feature = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($feature);
    			$em->flush();
    	
    			$this->get('session')->setFlash('notice', 'feature Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_features'));
    		}
    	}
    	
    	 
    	return array('feature'=>$feature,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deletefeature/{id}", name="_delete_feature")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formfeaturedelete.html.twig")
     */
    public function deletefeatureAction($id)
    {
    	$feature = $this->getDoctrine()->getRepository('GraceWebBundle:Feature')->find($id);
    	if (!$feature) $this->createNotFoundException('feature not found');
    	$form = $this->get('form.factory')->create(new FeatureType(),$feature);
    	
    	$request = $this->get('request');
    	
    	if ('POST' == $request->getMethod())
    	{
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($feature);
    		$em->flush();
    		
    		$this->get('session')->setFlash('notice', 'Feature Deleted!');
    		
    		return $this->redirect($this->generateUrl('_admin_features'));
    	}
    	
    	return array('form'=>$form->createView(),'feature'=>$feature);
    }
    
    /**
     * @Route("/editfeaturecomment/{id}", name="_edit_featurecomment")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formfeaturecommentedit.html.twig")
     */
    public function editfeaturecommentAction($id)
    {
    	$FeatureComment = $this->getDoctrine()->getRepository('GraceWebBundle:FeatureComment')->find($id);
    	if (!$FeatureComment) $this->createNotFoundException('feature not found');
    	$form = $this->get('form.factory')->create(new FeatureCommentType(),$FeatureComment);
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$FeatureComment = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($FeatureComment);
    			$em->flush();
    	
    			$this->get('session')->setFlash('notice', 'comment Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_feature',array('id'=>$FeatureComment->getFeature()->getId())));
    		}
    	}
    	
    	 
    	return array('comment'=>$FeatureComment,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deletefeaturecomment/{id}", name="_delete_featurecomment")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formfeaturecommentdelete.html.twig")
     */
    public function deleteFeatureCommentAction($id)
    {
    	$FeatureComment = $this->getDoctrine()->getRepository('GraceWebBundle:FeatureComment')->find($id);
    	if (!$FeatureComment) $this->createNotFoundException('comment not found');
    	$form = $this->get('form.factory')->create(new FeatureCommentType(),$FeatureComment);
    	
    	$request = $this->get('request');
    	
    	if ('POST' == $request->getMethod())
    	{
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($FeatureComment);
    		$em->flush();
    		
    		$this->get('session')->setFlash('notice', 'Comment Deleted!');
    		
    		return $this->redirect($this->generateUrl('_admin_feature',array('id'=>$FeatureComment->getFeature()->getId())));
    	}
    	
    	return array('form'=>$form->createView(),'comment'=>$FeatureComment);
    }
    
    /**
    * @Route("/articles", name="_admin_articles")
    * @Secure(roles="ROLE_ADMIN")
    * @Template("")
    */
    public function articlesAction()
    {
    	$articles = $this->getDoctrine()->getRepository('GraceWebBundle:Article')->findAll();
    	if (!$articles) $this->createNotFoundException('No articles were found');
    	return array('articles'=>$articles);
    }
    
    /**
     * @Route("/addarticles", name="_add_article")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formarticleadd.html.twig")
     */
    public function addarticlesAction()
    {
    	$article = new article();
    	$user = $this->get('security.context')->getToken()->getUser();
    	$article->setProfile($user->getProfile());
    	$article->setDate(new \DateTime());
    	$form = $this->get('form.factory')->create(new articleType(),$article);
    	$form->remove('noimage');
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$article = $form->getData();
    			$article->setCreator($user);
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($article);
    			$em->flush();
    
    			$this->get('session')->setFlash('notice', 'article Saved!');
    
    			return $this->redirect($this->generateUrl('_admin_articles'));
    		}
    	}
    
    	return array('form' => $form->createView());
    }
    
    /**
     * @Route("/editarticle/{id}", name="_edit_article")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formarticleedit.html.twig")
     */
    public function editarticleAction($id)
    {
    	$article = $this->getDoctrine()->getRepository('GraceWebBundle:Article')->find($id);
    	if (!$article) $this->createNotFoundException('article not found');
    	$form = $this->get('form.factory')->create(new articleType(),$article);
    	 
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$article = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($article);
    			$em->flush();
    			 
    			$this->get('session')->setFlash('notice', 'article Saved!');
    			 
    			return $this->redirect($this->generateUrl('_admin_articles'));
    		}
    	}
    	 
    
    	return array('article'=>$article,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deletearticle/{id}", name="_delete_article")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formarticledelete.html.twig")
     */
    public function deletearticleAction($id)
    {
    	$article = $this->getDoctrine()->getRepository('GraceWebBundle:Article')->find($id);
    	if (!$article) $this->createNotFoundException('article not found');
    	$form = $this->get('form.factory')->create(new ArticleType(),$article);
    	 
    	$request = $this->get('request');
    	 
    	if ('POST' == $request->getMethod())
    	{
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($article);
    		$em->flush();
    
    		$this->get('session')->setFlash('notice', 'Article Deleted!');
    
    		return $this->redirect($this->generateUrl('_admin_articles'));
    	}
    	 
    	return array('form'=>$form->createView(),'article'=>$article);
    }
    
    /**
     * @Route("/audiofiles", name="_admin_audiofiles")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("")
     */
    public function audiofilesAction()
    {
	    $AudioFiles = $this->getDoctrine()->getRepository('GraceWebBundle:AudioFile')->findAll();
	        	if (!$AudioFiles) $this->createNotFoundException('No audiofiles were found');
	    return array('audiofiles'=>$AudioFiles);
    }
    
    /**
     * @Route("/addaudiofiles", name="_add_audiofile")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formaudiofileadd.html.twig")
     */
    public function addAudioFilesAction()
    {
    	
    	$request = $this->getRequest();
    	$session = $request->getSession();
    	$cookies = $request->cookies;
    	$service_id = $session->get('service_id')>0 ? $session->get('service_id') : ($cookies->has('service_id') ? $cookies->get('service_id') : 1);
    	
    	$AudioFile = new AudioFile();
    	$serviceobj = $this->getDoctrine()->getRepository('GraceWebBundle:Service')->find($service_id);
    	$AudioFile->setService($serviceobj);
    	$form = $this->get('form.factory')->create(new AudioFileType(),$AudioFile);
    	
    	$request = $this->get('request');
        if ('POST' == $request->getMethod()) {
            $form->bindRequest($request);
            if ($form->isValid()) {
				$AudioFile = $form->getData();
            	$em = $this->getDoctrine()->getEntityManager();
            	$em->persist($AudioFile);
            	$em->flush();

                $this->get('session')->setFlash('notice', 'Audio File Saved!');

                return $this->redirect($this->generateUrl('_admin_audiofiles'));
            }
        }

        return array('form' => $form->createView());
    }
    
    /**
     * @Route("/editaudiofile/{id}", name="_edit_audiofile")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formaudiofileedit.html.twig")
     */
    public function editaudiofileAction($id)
    {
    	$AudioFile = $this->getDoctrine()->getRepository('GraceWebBundle:AudioFile')->find($id);
    	if (!$AudioFile) $this->createNotFoundException('Audio file not found');
    	$form = $this->get('form.factory')->create(new AudioFileType(),$AudioFile);
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$AudioFile = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($AudioFile);
    			$em->flush();
    			 
    			$mailer = $this->get('mailer');
    			// .. setup a message and send it
    			// http://symfony.com/doc/current/cookbook/email.html
    	
    			$this->get('session')->setFlash('notice', 'audiofile Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_audiofiles'));
    		}
    	}
    	
    	 
    	return array('audiofile'=>$AudioFile,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deleteaudiofile/{id}", name="_delete_audiofile")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formaudiofiledelete.html.twig")
     */
    public function deleteaudiofileAction($id)
    {
    	$AudioFile = $this->getDoctrine()->getRepository('GraceWebBundle:AudioFile')->find($id);
    	if (!$AudioFile) $this->createNotFoundException('audiofile not found');
    	$form = $this->get('form.factory')->create(new AudioFileType(),$AudioFile);
    	
    	$request = $this->get('request');
    	
    	if ('POST' == $request->getMethod())
    	{
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($AudioFile);
    		$em->flush();
    		
    		$this->get('session')->setFlash('notice', 'Audio File Deleted!');
    		
    		return $this->redirect($this->generateUrl('_admin_audiofiles'));
    	}
    	
    	return array('audiofile'=>$AudioFile,'form'=>$form->createView());
    }
    
    /**
     * @Route("/resourcecategories", name="_admin_resourceCategories")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("")
     */
    public function resourceCategoriesAction()
    {
	    $resourceCategories = $this->getDoctrine()->getRepository('GraceWebBundle:ResourceCategory')->findAll();
	        	if (!$resourceCategories) $this->createNotFoundException('No resource categories were found');
	    return array('resourceCategories'=>$resourceCategories);
    }
    
    /**
     * @Route("/addresourcecategory", name="_add_resourcecategory")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formresourcecategoryadd.html.twig")
     */
    public function addresourceCategoryAction()
    {
    	$resourceCategory = new resourceCategory();
    	$form = $this->get('form.factory')->create(new ResourceCategoryType(),$resourceCategory);
    	    	
    	$request = $this->get('request');
        if ('POST' == $request->getMethod()) {
            $form->bindRequest($request);
            if ($form->isValid()) {
				$resourceCategory = $form->getData();
            	$em = $this->getDoctrine()->getEntityManager();

                // get and set the last list_order
                $qb = $em->createQueryBuilder();
                $qr = $qb->add('select',$qb->expr()->max('c.listOrder'))
                ->add('from','GraceWebBundle:ResourceCategory c')
                ->setMaxResults(1)->getQuery()->getSingleResult();
                $resourceCategory->setListOrder($qr[1] + 1);
                
            	$em->persist($resourceCategory);
            	$em->flush();

                $this->get('session')->setFlash('notice', 'resource category Saved!');

                return $this->redirect($this->generateUrl('_admin_resourceCategories'));
            }
        }

        return array('form' => $form->createView());
    }
    
    /**
     * @Route("/editresourceategory/{id}", name="_edit_resourcecategory")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formresourcecategoryedit.html.twig")
     */
    public function editresourceCategoryAction($id)
    {
    	$resourceCategory = $this->getDoctrine()->getRepository('GraceWebBundle:ResourceCategory')->find($id);
    	if (!$resourceCategory) $this->createNotFoundException('Resource category not found');
    	$form = $this->get('form.factory')->create(new ResourceCategoryType(),$resourceCategory);
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$resourceCategory = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($resourceCategory);
    			$em->flush();
    			 
    			$mailer = $this->get('mailer');
    			// .. setup a message and send it
    			// http://symfony.com/doc/current/cookbook/email.html
    	
    			$this->get('session')->setFlash('notice', 'Resource category Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_resourceCategories'));
    		}
    	}
    	
    	 
    	return array('resourceCategory'=>$resourceCategory,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deleteresourcecategory/{id}", name="_delete_resourcecategory")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formresourcecategorydelete.html.twig")
     */
    public function deleteresourceCategoryAction($id)
    {
    	$resourceCategory = $this->getDoctrine()->getRepository('GraceWebBundle:ResourceCategory')->find($id);
    	if (!$resourceCategory) $this->createNotFoundException('Resource category not found');
    	$form = $this->get('form.factory')->create(new ResourceCategoryType(),$resourceCategory);
    	 
    	$request = $this->get('request');
    	 
    	if ('POST' == $request->getMethod())
    	{
    		$em = $this->getDoctrine()->getEntityManager();
    		// Remove associations with Events and Cells (other relationships should be cascade remove)
    		foreach ($resourceCategory->getFiles() as $file) $file->setresourceCategory(NULL);
    		$em->remove($resourceCategory);
    		$em->flush();
    
    		$this->get('session')->setFlash('notice', 'Profile Deleted!');
    
    		return $this->redirect($this->generateUrl('_admin_resourceCategories'));
    	}
    	 
    	return array('form'=>$form->createView(),'resourceCategory'=>$resourceCategory);
    }
    
    /**
     * @Route("/resourcefiles", defaults={"category_id"=0}, name="_admin_resourcefiles")
     * @Route("/resourcefiles/{category_id}", defaults={"category_id"=0}, name="_admin_resourcefiles_category")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("")
     */
    public function resourcesAction($category_id)
    {
    	$ResourceCategory = $this->getDoctrine()->getRepository('GraceWebBundle:ResourceCategory')->find($category_id);
    	if (!$ResourceCategory) $this->createNotFoundException('Resource category not found');
    	return array('category'=>$ResourceCategory);
    }
    
    /**
     * @Route("/addresourcefiles", defaults={"id"=0}, name="_add_resourcefile")
     * @Route("/addresourcefiles/{id}", defaults={"id"=0}, name="_add_resourcefile_category")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formresourcefileadd.html.twig")
     */
    public function addResourceFilesAction($id)
    {
        if ($id) $ResourceCategory = $this->getDoctrine()->getRepository('GraceWebBundle:ResourceCategory')->find($id);
    	$ResourceFile = new ResourceFile();
        if ($id) $ResourceFile->setCategory($ResourceCategory);
    	$form = $this->get('form.factory')->create(new ResourceFileType(),$ResourceFile);
    	 
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$ResourceFile = $form->getData();
    			$cat_id = $ResourceFile->getCategory()->getID();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($ResourceFile);
    			$em->flush();
    
    			$this->get('session')->setFlash('notice', 'Resource File Saved!');
    
    			return $this->redirect($this->generateUrl('_admin_resourcefiles_category',array('category_id'=>$ResourceFile->getCategory()->getID())));
    		}
    	}
    
    	return array('form' => $form->createView());
    }
    
    /**
     * @Route("/editresourcefile/{id}", name="_edit_resourcefile")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formresourcefileedit.html.twig")
     */
    public function editresourcefileAction($id)
    {
    	$ResourceFile = $this->getDoctrine()->getRepository('GraceWebBundle:ResourceFile')->find($id);
    	if (!$ResourceFile) $this->createNotFoundException('Resource file not found');
    	$form = $this->get('form.factory')->create(new ResourceFileType(),$ResourceFile);
    	 
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$ResourceFile = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($ResourceFile);
    			$em->flush();
    			 
    			$this->get('session')->setFlash('notice', 'resourcefile Saved!');
    			 
                return $this->redirect($this->generateUrl('_admin_resourcefiles_category',array('category_id'=>$ResourceFile->getCategory()->getID())));
    		}
    	}
    	 
    
    	return array('resourcefile'=>$ResourceFile,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deleteresourcefile/{id}", name="_delete_resourcefile")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formresourcefiledelete.html.twig")
     */
    public function deleteresourcefileAction($id)
    {
    	$ResourceFile = $this->getDoctrine()->getRepository('GraceWebBundle:ResourceFile')->find($id);
    	if (!$ResourceFile) $this->createNotFoundException('resourcefile not found');
    	$form = $this->get('form.factory')->create(new ResourceFileType(),$ResourceFile);
    	 
    	$request = $this->get('request');
    	 
    	if ('POST' == $request->getMethod())
    	{
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($ResourceFile);
    		$em->flush();
    
    		$this->get('session')->setFlash('notice', 'Resource File Deleted!');
    
            return $this->redirect($this->generateUrl('_admin_resourcefiles_category',array('category_id'=>$ResourceFile->getCategory()->getID())));
    	}
    	 
    	return array('resourcefile'=>$ResourceFile,'form'=>$form->createView());
    }
    
    /**
     * @Route("/ministries", name="_admin_ministries")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("")
     */
    public function ministriesAction()
    {
	    $ministries = $this->getDoctrine()->getRepository('GraceWebBundle:Ministry')->findAll();
	    if (!$ministries) $this->createNotFoundException('No ministries were found');
	    return array('ministries'=>$ministries);
    }
    
    /**
     * @Route("/addministry", name="_add_ministry")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formministryadd.html.twig")
     */
    public function addministryAction()
    {
    	$ministry = new ministry();
    	$form = $this->get('form.factory')->create(new ministryType($ministry));
    	    	
    	$request = $this->get('request');
        if ('POST' == $request->getMethod()) {
            $form->bindRequest($request);
            if ($form->isValid()) {
				$ministry = $form->getData();
            	$em = $this->getDoctrine()->getEntityManager();
            	
            	$qb = $em->createQueryBuilder();
            	$qr = $qb->add('select',$qb->expr()->max('m.list_order'))
            	->add('from','GraceWebBundle:Ministry m')
            	->setMaxResults(1)->getQuery()->getSingleResult();
            	$ministry->setListOrder($qr[1] + 1);
            	
            	$em->persist($ministry);
            	$em->flush();

                $this->get('session')->setFlash('notice', 'ministry Saved!');

                return $this->redirect($this->generateUrl('_admin_ministries'));
            }
        }

        return array('form' => $form->createView());
    }
    
    /**
     * @Route("/editministry/{id}", name="_edit_ministry")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formministryedit.html.twig")
     */
    public function editministryAction($id)
    {
    	$ministry = $this->getDoctrine()->getRepository('GraceWebBundle:Ministry')->find($id);
    	if (!$ministry) $this->createNotFoundException('ministry not found');
    	$form = $this->get('form.factory')->create(new ministryType(),$ministry);
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$ministry = $form->getData();
    			$ministry->preUpload();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($ministry);
    			$em->flush();
    	
    			$this->get('session')->setFlash('notice', 'ministry Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_ministries'));
    		}
    	}
    	
    	 
    	return array('ministry'=>$ministry,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deleteministry/{id}", name="_delete_ministry")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formministrydelete.html.twig")
     */
    public function deleteministryAction($id)
    {
    	$ministry = $this->getDoctrine()->getRepository('GraceWebBundle:Ministry')->find($id);
    	if (!$ministry) $this->createNotFoundException('ministry not found');
    	$form = $this->get('form.factory')->create(new MinistryType());
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    			
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($ministry);
    		$em->flush();
    	
    		$this->get('session')->setFlash('notice', 'ministry deleted!');
    	
    		return $this->redirect($this->generateUrl('_admin_ministries'));
    	}
    	
    	return array('form'=>$form->createView(),'ministry'=>$ministry);
    }
    
    /**
     * @Route("/cells", name="_admin_cells")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("")
     */
    public function cellsAction()
    {
    	$cells = $this->getDoctrine()->getRepository('GraceWebBundle:Cell')->findAllAdmin();
	    if (!$cells) $this->createNotFoundException('No cells were found');
	    return array('cells'=>$cells);
    }
    
    /**
     * @Route("/addcell", name="_add_cell")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formcelladd.html.twig")
     */
    public function addcellAction()
    {
    	$cell = new cell();
    	$form = $this->get('form.factory')->create(new cellType($cell));
    	    	
    	$request = $this->get('request');
        if ('POST' == $request->getMethod()) {
            $form->bindRequest($request);
            if ($form->isValid()) {
				$cell = $form->getData();
            	$em = $this->getDoctrine()->getEntityManager();
            	
            	$qb = $em->createQueryBuilder();
            	$qr = $qb->add('select',$qb->expr()->max('c.list_order'))
            	->add('from','GraceWebBundle:Cell c')
            	->setMaxResults(1)->getQuery()->getSingleResult();
            	$cell->setListOrder($qr[1] + 1);
            	
            	$em->persist($cell);
            	$em->flush();

                $this->get('session')->setFlash('notice', 'cell Saved!');

                return $this->redirect($this->generateUrl('_admin_cells'));
            }
        }

        return array('form' => $form->createView());
    }
    
    /**
     * @Route("/editcell/{id}", name="_edit_cell")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formcelledit.html.twig")
     */
    public function editcellAction($id)
    {
    	$cell = $this->getDoctrine()->getRepository('GraceWebBundle:Cell')->find($id);
    	if (!$cell) $this->createNotFoundException('cell not found');
    	$form = $this->get('form.factory')->create(new cellType(),$cell);
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$cell = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($cell);
    			$em->flush();
    	
    			$this->get('session')->setFlash('notice', 'cell Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_cells'));
    		}
    	}
    	
    	 
    	return array('cell'=>$cell,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deletecell/{id}", name="_delete_cell")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formcelldelete.html.twig")
     */
    public function deletecellAction($id)
    {
    	$cell = $this->getDoctrine()->getRepository('GraceWebBundle:Cell')->find($id);
    	if (!$cell) $this->createNotFoundException('cell not found');
    	$form = $this->get('form.factory')->create(new CellType());
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    			
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($cell);
    		$em->flush();
    	
    		$this->get('session')->setFlash('notice', 'cell deleted!');

    		return $this->redirect($this->generateUrl('_admin_cells'));
    	}
    	
    	return array('form'=>$form->createView(),'cell'=>$cell);
    }
    
    
    /**
    * @Route("/users", name="_admin_users")
     * @Secure(roles="ROLE_SUPER_ADMIN")
    * @Template("")
    */
    public function usersAction()
    {
	    $users = $this->getDoctrine()->getRepository('GraceWebBundle:User')->findAll();
	    	        	if (!$users) $this->createNotFoundException('No users were found');
	    return array('users'=>$users);
    }
    
    /**
    * @Route("/adduser", name="_add_user")
    * @Secure(roles="ROLE_SUPER_ADMIN")
     * @Template("GraceWebBundle:Secured:formuseradd.html.twig")
     */
     public function adduserAction()
     {
	     $user = new user();
	     $form = $this->get('form.factory')->create(new userType(),$user);
    
 	     $request = $this->get('request');
	     if ('POST' == $request->getMethod()) 
	     {
	     	$form->bindRequest($request);
            if ($form->isValid()) 
            {
            	$factory = $this->get('security.encoder_factory');
    			$user = $form->getData();
    			$encoder = $factory->getEncoder($user);
    			$password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
    			$user->setPassword($password);
		    	$em = $this->getDoctrine()->getEntityManager();
		    	$em->persist($user);
		    	$em->flush();
    	 	
		    	$mailer = $this->get('mailer');
		    	// .. setup a message and send it
		    	// http://symfony.com/doc/current/cookbook/email.html
    
    			$this->get('session')->setFlash('notice', 'user Saved!');
    
    			return $this->redirect($this->generateUrl('_admin_users'));
    		}
    	}
    
    	return array('form' => $form->createView());
    }
    
    /**
    * @Route("/edituser/{id}", name="_edit_user")
    * @Secure(roles="ROLE_SUPER_ADMIN")
    * @Template("GraceWebBundle:Secured:formuseredit.html.twig")
    */
    public function edituserAction($id)
    {
    	$user = $this->getDoctrine()->getRepository('GraceWebBundle:User')->find($id);
    	if (!$user) $this->createNotFoundException('user not found');
    	$form = $this->get('form.factory')->create(new userType(),$user);
    	$form->remove('password');
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) 
    	{
    		$form->bindRequest($request);
        	if ($form->isValid()) 
        	{
		    	$user = $form->getData();
		    	$em = $this->getDoctrine()->getEntityManager();
		    	$em->persist($user);
		        $em->flush();
		    	 
		    	$this->get('session')->setFlash('notice', 'User Saved!');
		    	 
		    	return $this->redirect($this->generateUrl('_admin_users'));
		    }
    	}
    	 
    
    	return array('user'=>$user,'form'=>$form->createView());
    }
    
    /**
     * @Route("/changepw/{id}", name="_changepw")
     * @Secure(roles="ROLE_SUPER_ADMIN")
     * @Template("GraceWebBundle:Secured:formuserpwchange.html.twig")
     */
    public function userpwchangeAction($id)
    {
    	$user = $this->getDoctrine()->getRepository('GraceWebBundle:User')->find($id);
    	if (!$user) $this->createNotFoundException('user not found');
    	$form = $this->createFormBuilder($user)
			->add('password', 'password', array(
					'label'=> 'New password',
			))->getForm();
					
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod())
    	{
    		$form->bindRequest($request);
    		if ($form->isValid())
    		{
    			$factory = $this->get('security.encoder_factory');
    			$user = $form->getData();
    			$encoder = $factory->getEncoder($user);
    			$password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
    			$user->setPassword($password);
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($user);
    			$em->flush();
    
    			$this->get('session')->setFlash('notice', 'User\'s password Saved!');
    
    			return $this->redirect($this->generateUrl('_admin_users'));
    		}
    	}
    
    
    	return array('user'=>$user,'form'=>$form->createView());
    }
    
    /**
     * @Route("/changemypw", name="_changemypw")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formchangemypw.html.twig")
     */
    public function changeMyPWAction()
    {
    	$user = $this->get('security.context')->getToken()->getUser();
    	if (!$user) $this->createNotFoundException('user not found');
    	$form = $this->createFormBuilder($user)
    	->add('password', 'repeated', array(
    			'type'=>'password',
    			'invalid_message'=>'The password fields must match.',
    			'first_name'=> 'New password',
    			'second_name'=>'Repeat new password'))
    	->getForm();
    		
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod())
    	{
    		$form->bindRequest($request);
    		if ($form->isValid())
    		{
    			$factory = $this->get('security.encoder_factory');
    			$user = $form->getData();
    			$encoder = $factory->getEncoder($user);
    			$password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
    			$user->setPassword($password);
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($user);
    			$em->flush();
    
    			$mailer = $this->get('mailer');
    			// .. setup a message and send it
    			// http://symfony.com/doc/current/cookbook/email.html
    
    			$this->get('session')->setFlash('notice', 'You changed your password!');
    
    			return $this->redirect($this->generateUrl('_admin_index'));
    		}
    	}
    
    
    	return array('user'=>$user,'form'=>$form->createView());
    }
    
    /**
    * @Route("/deleteuser/{id}", name="_delete_user")
    * @Secure(roles="ROLE_SUPER_ADMIN")
    * @Template("GraceWebBundle:Secured:formuserdelete.html.twig")
    */
    public function deleteuserAction($id)
    {
    	$user = $this->getDoctrine()->getRepository('GraceWebBundle:User')->find($id);
    	if (!$user) $this->createNotFoundException('user not found');
    	$form = $this->get('form.factory')->create(new UserType());
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		
    			
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->remove($user);
    			$em->flush();
    	
    			$this->get('session')->setFlash('notice', 'User deleted!');
    	
    			return $this->redirect($this->generateUrl('_admin_users'));
    	}
    	
    	 
    	return array('form'=>$form->createView(), 'user'=>$user);
    }
    
    /**
     * @Route("/rosterroles", defaults={"service_id"=0})
     * @Route("/rosterroles/{service_id}", defaults={"service_id"=0}, name="_admin_rosterroles")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("")
     */
    public function rosterrolesAction($service_id)
    {
    	
    	$request = $this->getRequest();
    	$session = $request->getSession();
    	$cookies = $request->cookies;
    	if ($service_id == 0)
    		$service_id = $session->get('service_id')>0 ? $session->get('service_id') : ($cookies->has('service_id') ? $cookies->get('service_id') : 1);
    	$session->set('service',$service_id);
    	$cookie = new Cookie('service_id', $service_id, (time() + 3600 * 24 * 7),$this->generateUrl('_roster'));
    	$response = new Response();
    	$response->headers->setCookie($cookie);
    	$response->send();
    	 
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('c','s')
    	->add('from','GraceWebBundle:Congregation c')
    	->leftJoin('c.services','s')
    	->orderBy('c.id','ASC');
    	$q = $qb->getQuery();
    	$congregations = $q->getResult();
    	
    	$rosterroles = $this->getDoctrine()->getRepository('GraceWebBundle:RosterRole')->findByService($service_id);
	    if (!$rosterroles) $this->createNotFoundException('No rosterroles were found');
	    return array('rosterroles'=>$rosterroles,'congregations'=>$congregations,'curservice'=>$service_id);
    }
    
    /**
     * @Route("/addrosterrole/{service}", defaults={"service"=1}, name="_add_rosterrole")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formrosterroleadd.html.twig")
     */
    public function addrosterrolesAction($service)
    {
    	$serviceobj = $this->getDoctrine()->getRepository('GraceWebBundle:Service')->find($service);
    	$rosterrole = new rosterrole();
    	$form = $this->get('form.factory')->create(new rosterroleType(),$rosterrole);
    	    	
    	$request = $this->get('request');
        if ('POST' == $request->getMethod()) {
            $form->bindRequest($request);
            if ($form->isValid()) {
				$rosterrole = $form->getData();
    			$rosterrole->setService($serviceobj);
            	$em = $this->getDoctrine()->getEntityManager();
            	
            	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
            	$qr = $qb->add('select',$qb->expr()->max('r.list_order'))            	
            	->add('from','GraceWebBundle:RosterRole r')
            	->add('where','r.service = ?1')
            	->setMaxResults(1)
            	->setParameter(1,$service)
            	->getQuery()->getSingleResult();
            	$rosterrole->setListOrder($qr[1] + 1);
            	
            	$em->persist($rosterrole);
            	$em->flush();

                $this->get('session')->setFlash('notice', 'rosterrole Saved!');

                return $this->redirect($this->generateUrl('_admin_rosterroles'));
            }
        }

        return array('form' => $form->createView(),'serviceid'=>$service);
    }
    
    /**
     * @Route("/editrosterrole/{id}", name="_edit_rosterrole")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formrosterroleedit.html.twig")
     */
    public function editrosterroleAction($id)
    {
    	$rosterrole = $this->getDoctrine()->getRepository('GraceWebBundle:RosterRole')->find($id);
    	if (!$rosterrole) $this->createNotFoundException('rosterrole not found');
    	$form = $this->get('form.factory')->create(new rosterroleType(),$rosterrole);
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$rosterrole = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($rosterrole);
    			$em->flush();
    	
    			$this->get('session')->setFlash('notice', 'rosterrole Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_rosterroles'));
    		}
    	}
    	
    	 
    	return array('rosterrole'=>$rosterrole,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deleterosterrole/{id}", name="_delete_rosterrole")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formrosterroledelete.html.twig")
     */
    public function deleterosterroleAction($id)
    {
    	$rosterrole = $this->getDoctrine()->getRepository('GraceWebBundle:RosterRole')->find($id);
    	if (!$rosterrole) $this->createNotFoundException('rosterrole not found');
    	$form = $this->get('form.factory')->create(new RosterroleType());
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    	
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($rosterrole);
    		$em->flush();
    		 
    		$this->get('session')->setFlash('notice', 'roster role deleted!');
    		 
    		return $this->redirect($this->generateUrl('_admin_rosterroles'));
    	}    	
    	
    	return array('rosterrole'=>$rosterrole,'form'=>$form->createView());
    }
    
    /**
    * @Route("/roster", defaults={"service_id"=0,"year"=0,"term"=0}, name="_admin_rostera")
    * @Route("/roster/{service_id}", defaults={"service_id"=0,"year"=0,"term"=0}, name="_admin_roster")
    * @Route("/roster/{service_id}/{year}/{term}", name="_admin_rostert")
    * @Secure(roles="ROLE_ADMIN")
    * @Template()
    */
    public function rosterAction($service_id,$year,$term)
    {
    	
    	$request = $this->getRequest();
    	$session = $request->getSession();
    	$cookies = $request->cookies;
    	if ($service_id == 0)
    		$service_id = $session->get('service_id')>0 ? $session->get('service_id') : ($cookies->has('service_id') ? $cookies->get('service_id') : 1);
    	if ($year == 0) 
    		$year = $session->get('year')>0 ? $session->get('year') : date('Y');
    	
    	if ($term == 0) 
    		if ($session->get('term')) $term = $session->get('term');
    	else
    	{
	    	// Figure out the right term.
	    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
	    	$qb->select('l')
	    		->add('from','GraceWebBundle:RosterLine l')
	    		->where(
	    			$qb->expr()->andx(
	    				$qb->expr()->eq('l.service','?1'),
	    				$qb->expr()->gt('l.date','?2')
	    			)
	    		)
	    		->orderBy('l.date','ASC')
	    		->setParameter(1,$service_id)
	    		->setParameter(2,date('Y-m-d'))
	    		->setMaxResults(1);
	    	$q = $qb->getQuery();
	    	
	    	try {
	    		$nextrosteritem = $q->getSingleResult();
	    		$term = $nextrosteritem->getTerm();
	    	} catch (\Exception $e) {
	    		$term = 1;
	    	}
	    	
    	}
    	
    	$session->set('service_id',$service_id);
    	$session->set('year',$year);
    	$session->set('term',$term);
    	$cookie = new Cookie('service_id', $service_id, (time() + 3600 * 24 * 7),$this->generateUrl('_admin_rostera'));
    	$response = new Response();
    	$response->headers->setCookie($cookie);
    	$response->send();
    	
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('c','s')
    	->add('from','GraceWebBundle:Congregation c')
    	->leftJoin('c.services','s')
    	->orderBy('c.id','ASC');
    	$q = $qb->getQuery();
    	$congregations = $q->getResult();
    	 
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('r')
    	->add('from','GraceWebBundle:RosterRole r')
    	->where($qb->expr()->eq('r.service','?1'))
    	->orderBy('r.list_order','ASC')
    	->setParameter(1,$service_id);
    	$q = $qb->getQuery();
    	$roles = $q->getResult();
    	 
    	$role_ids = array(0);
    	foreach ($roles as $role) $role_ids[] = $role->getID();
    	 
    	//$query = $this->getDoctrine()->getentitymanager()->createQuery('SELECT l, i, r from GraceWebBundle:RosterLine l left join l.rosteritems i, i.rosterroles r');
    	 
    	$startmonth = 1 + (($term -1) * 3);
    	$endmonth = $startmonth + 3;
    	$endyear = $endmonth > 12 ? $year + 1 : $year;
    	$endmonth = $endmonth > 12 ? 1 : $endmonth;
    	 
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('l','i','r','p')
    	->add('from','GraceWebBundle:RosterLine l')
    	->leftJoin('l.rosteritems','i')
    	->leftJoin('i.rosterrole','r')
    	->leftJoin('i.person','p')
    	->add(
        			'where',
    	$qb->expr()->andx(
    	$qb->expr()->orx(
    	$qb->expr()->in('r.id',$role_ids),
    	    				'i.id is NULL'
    	),
    	$qb->expr()->between('l.date','?1','?2'),
    	$qb->expr()->eq('l.term','?3'),
    	$qb->expr()->eq('l.service','?4')
    	)
    	)
    	->orderBy('l.date','ASC')
    	->add('orderBy','r.list_order','ASC');
    	$q = $qb->getQuery();
    	$q->setParameter(1,$year.'-01-01');
    	$q->setParameter(2,$year.'-12-31');
    	$q->setParameter(3,$term);
    	$q->setParameter(4,$service_id);
    	$lines = $q->getResult();
    	
    	$rosterLine = new RosterLine();
    	if (count($lines)) $rosterLine->setDate($lines[0]->getDate());
    	$serviceobj = $this->getDoctrine()->getRepository('GraceWebBundle:Service')->find($service_id);
    	if ($serviceobj) $rosterLine->setService($serviceobj);
    	$rosterLine->setTerm($term);
    	$form = $this->createFormBuilder($rosterLine)
        ->add('date','date',array('widget'=>'single_text','format'=>'dd/MM/yyyy'))
        ->add('excursion')
        ->getForm();
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod())
    	{
    		$form->bindRequest($request);
    		if ($form->isValid())
    		{
    			$rosterLine = $form->getData();
    			$rosterLine->setTerm($term);
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($rosterLine);
    			$em->flush();
    	
    			$this->get('session')->setFlash('notice', 'Roster line Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_roster'));
    		}
    	}
    	 
    	return array(
        		'congregations'=>$congregations,
        		'roles'=>$roles,
        		'lines'=>$lines,
        		'form'=>$form->createView(),
        		'meta'=>array(
        			'year'=>$year,
        			'nextyear'=>$year+1,
        			'prevyear'=>$year-1,
        			'term'=>$term,
        			'nextterm'=>($term+1>4?1:$term+1),
        			'nexttermyear'=>($term+1>4?$year+1:$year),
        			'prevterm'=>($term-1<1?4:$term-1),
        			'prevtermyear'=>($term-1<1?$year-1:$year),
        			'service'=>$service_id)
    	);
    }
    
    /**
    * @Route("/addrosteritems/{rosterRole}/{rosterLine}", defaults={"rosterRole"=1,"rosterLine"=1}, name="_add_rosteritem")
    * @Secure(roles="ROLE_ADMIN")
    * @Template("GraceWebBundle:Secured:formrosteritemadd.html.twig")
    */
    public function addrosteritemsAction($rosterRole,$rosterLine)
    {
    	$rosterItem = new rosterItem();
    	$request = $this->get('request');
    	
    	$roleobj = $this->getDoctrine()->getRepository('GraceWebBundle:RosterRole')->find($rosterRole);
    	$lineobj = $this->getDoctrine()->getRepository('GraceWebBundle:RosterLine')->find($rosterLine);
    	$rosterItem->setRosterrole($roleobj);
    	$rosterItem->setRosterline($lineobj);
    	
	    if ($roleobj->getType()=='text') $form = $this->get('form.factory')->create(new rosterItemTextType(),$rosterItem);
	    else $form = $this->get('form.factory')->create(new rosterItemPersonType(),$rosterItem);
    	
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$rosterItem = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($rosterItem);
    			$em->flush();
    
    			$this->get('session')->setFlash('notice', 'roster item Saved!');
    
    			return $this->redirect($this->generateUrl('_admin_roster'));
    		}
    	}
    
    	return array('form' => $form->createView(),'rosterItem'=>$rosterItem);
    }
    
    /**
     * @Route("/editrosteritem/{id}", name="_edit_rosteritem")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formrosteritemedit.html.twig")
     */
    public function editrosteritemAction($id)
    {
    	$rosterItem = $this->getDoctrine()->getRepository('GraceWebBundle:RosterItem')->find($id);
    	if (!$rosterItem) $this->createNotFoundException('roster item not found');
    	if ($rosterItem->getRosterrole()->getType()=='text') $form = $this->get('form.factory')->create(new rosterItemTextType(),$rosterItem);
    	else $form = $this->get('form.factory')->create(new rosterItemPersonType(),$rosterItem);
    	 
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$rosterItem = $form->getData();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($rosterItem);
    			$em->flush();
    			 
    			$this->get('session')->setFlash('notice', 'roster item Saved!');
    			 
    			return $this->redirect($this->generateUrl('_admin_roster'));
    		}
    	}
    	 
    
    	return array('rosterItem'=>$rosterItem,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deleterosteritem/{id}", name="_delete_rosteritem")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formrosteritemdelete.html.twig")
     */
    public function deleterosteritemAction($id)
    {
    	$rosterItem = $this->getDoctrine()->getRepository('GraceWebBundle:RosterItem')->find($id);
    	if (!$rosterItem) $this->createNotFoundException('roster item not found');
    	$form = $this->createFormBuilder($rosterItem)->getForm();
    	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->remove($rosterItem);
    			$em->flush();
    	
    			$mailer = $this->get('mailer');
    			// .. setup a message and send it
    			// http://symfony.com/doc/current/cookbook/email.html
    	
    			$this->get('session')->setFlash('notice', 'roster item Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_roster'));
    	}
    	
    	return array('rosterItem'=>$rosterItem,'form'=>$form->createView());
    }
    
    
    /**
    * @Route("/addrosterline", name="_add_rosterline")
     * @Secure(roles="ROLE_ADMIN")
    * @Template("GraceWebBundle:Secured:formrosterlineadd.html.twig")
    */
    public function addrosterlinesAction()
    {
    
    	$form = $this->get('form.factory')->create(new rosterLineType());
        
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) 
	   	{
       		$form->bindRequest($request);
   			if ($form->isValid()) 
   			{
		    	$rosterLine = $form->getData();
		    	$em = $this->getDoctrine()->getEntityManager();
		    	$em->persist($rosterLine);
		    	$em->flush();
   
		    	$mailer = $this->get('mailer');
		    	// .. setup a message and send it
		    	// http://symfony.com/doc/current/cookbook/email.html
		        
		    	$this->get('session')->setFlash('notice', 'roster line Saved!');
		    
		    	return $this->redirect($this->generateUrl('_admin_roster'));
    		}
    	}
    	return array('form'=>$form->createView());
   	}
    
    /**
     * @Route("/editrosterline/{id}", name="_edit_rosterline")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formrosterlineedit.html.twig")
     */
    public function editrosterlineAction($id)
    {
    	$rosterLine = $this->getDoctrine()->getRepository('GraceWebBundle:RosterLine')->find($id);
    	if (!$rosterLine) $this->createNotFoundException('roster line not found');
    	
    	$rosterRoles = $this->getDoctrine()->getRepository('GraceWebBundle:RosterRole')->findByService($rosterLine->getService());
    	if (!$rosterRoles) $this->createNotFoundException('no roster roles not found');
    	
    	$role_ids = array(); // of used role ids
    	foreach ($rosterLine->getRosterItems() as $item)
    	{
    		$role_id = $item->getRosterRole()->getId();
    		$role_ids[] = $role_id;
    		$items[$role_id] = $item;
    	}
    	$rosterLine->setRosterItems(new ArrayCollection());
    	foreach ($rosterRoles as $role)
    	{
    		$item = new RosterItem();
    		$item->setRosterRole($role);
    		$item->setRosterLine($rosterLine);
    		if (in_array($role->getId(), $role_ids)) $item = $items[$role->getId()];
    		$rosterLine->addRosterItem($item);
    		unset($item);
    	}
    	$form = $this->get('form.factory')->create(new rosterLineType(),$rosterLine);
    	    	     	
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$rosterLine = $form->getData();
    			
    			$em = $this->getDoctrine()->getEntityManager();
    			
    			$items = $rosterLine->getRosterItems();
    			foreach ($items as $n=>$item)
    			{
    				if (is_null($item->getPerson()) && is_null($item->getFreeText()))
    				{
    					if ($item->getId()) $em->remove($item);
    					unset($items[$n]);
    				}
    			}
    			$rosterLine->setRosterItems($items);
    			$em->persist($rosterLine);
    			$em->flush();
    			 
    			$this->get('session')->setFlash('notice', 'roster line Saved!');
    			 
    			return $this->redirect($this->generateUrl('_admin_roster'));
    		}
    	}
    	 
    
    	return array('rosterLine'=>$rosterLine,'form'=>$form->createView());
    }
   	
    
   	/**
   	* @Route("/deleterosterline/{id}", name="_delete_rosterline")
   	* @Secure(roles="ROLE_ADMIN")
   	* @Template("GraceWebBundle:Secured:formrosterlinedelete.html.twig")
   	*/
   	public function deleterosterlineAction($id)
    {
    	$rosterLine = $this->getDoctrine()->getRepository('GraceWebBundle:RosterLine')->find($id);
    	if (!$rosterLine) $this->createNotFoundException('roster line not found');
    	$form = $this->get('form.factory')->create(new RosterLineType(),$rosterLine);
    	 
    	$request = $this->get('request');
    	
    	if ('POST' == $request->getMethod()) 
    	{    
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($rosterLine);
    		$em->flush();
    		 
    		$mailer = $this->get('mailer');
    		// .. setup a message and send it
    		// http://symfony.com/doc/current/cookbook/email.html
    		 
    		$this->get('session')->setFlash('notice', 'roster item Saved!');
    		 
    		return $this->redirect($this->generateUrl('_admin_roster'));
    	}
    	 
    	return array('rosterline'=>$rosterLine,'form'=>$form->createView());
    }
    
    /**
     * @Route("/pages", name="_admin_pages")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("")
     */
    public function pagesAction()
    {
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('p')->add('from','GraceWebBundle:Page p')->orderBy('p.list_order','ASC');
    	$pages = $qb->getQuery()->getResult();
	    if (!$pages) $this->createNotFoundException('No pages were found');
	    return array('pages'=>$pages);
    }
    
    /**
     * @Route("/addpage", name="_add_page")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formpageadd.html.twig")
     */
    public function addpageAction()
    {
    	$page = new page();
    	$form = $this->get('form.factory')->create(new pageType($page));
    	    	
    	$request = $this->get('request');
        if ('POST' == $request->getMethod()) {
            
        	$form->bindRequest($request);
            if ($form->isValid()) {
            	
				$page = $form->getData();
    			$page->upload();
            	$em = $this->getDoctrine()->getEntityManager();
            	
            	$qb = $em->createQueryBuilder();
            	$qr = $qb->add('select',$qb->expr()->max('p.list_order'))
            	->add('from','GraceWebBundle:Page p')
            	->setMaxResults(1)->getQuery()->getSingleResult();
            	$page->setListOrder($qr[1] + 1);
            	
            	$em->persist($page);
            	$em->flush();

                $this->get('session')->setFlash('notice', 'page Saved!');

                return $this->redirect($this->generateUrl('_admin_pages'));
            }
        }

        return array('form' => $form->createView());
    }
    
    /**
     * @Route("/editpage/{id}", name="_edit_page")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formpageedit.html.twig")
     */
    public function editpageAction($id)
    {
    	$page = $this->getDoctrine()->getRepository('GraceWebBundle:Page')->find($id);
    	if (!$page) $this->createNotFoundException('page not found');
    	$form = $this->get('form.factory')->create(new pageType(),$page);
    	$request = $this->get('request');
    	if ('POST' == $request->getMethod()) {
    		$form->bindRequest($request);
    		if ($form->isValid()) {
    			$page = $form->getData();
    			$page->upload();
    			$em = $this->getDoctrine()->getEntityManager();
    			$em->persist($page);
    			$em->flush();
    	
    			$this->get('session')->setFlash('notice', 'page Saved!');
    	
    			return $this->redirect($this->generateUrl('_admin_pages'));
    		}
    	}
    	
    	 
    	return array('page'=>$page,'form'=>$form->createView());
    }
    
    /**
     * @Route("/deletepage/{id}", name="_delete_page")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("GraceWebBundle:Secured:formpagedelete.html.twig")
     */
    public function deletepageAction($id)
    {
    	$page = $this->getDoctrine()->getRepository('GraceWebBundle:Page')->find($id);
    	if (!$page) $this->createNotFoundException('Page not found');
    	$form = $this->get('form.factory')->create(new PageType(),$page);
    	 
    	$request = $this->get('request');
    	
    	if ('POST' == $request->getMethod()) 
    	{    
    		$em = $this->getDoctrine()->getEntityManager();
    		$em->remove($page);
    		$em->flush();
    		 
    		$this->get('session')->setFlash('notice', 'Page deleted');
    		 
    		return $this->redirect($this->generateUrl('_admin_pages'));
    	}
    	 
    	return array('page'=>$page,'form'=>$form->createView());
    }
    
    /**
     * @Route("/switchuser/{id}", name="_admin_switchuser")
     * @Secure(roles="ROLE_SUPER_ADMIN")
     */
    public function switchUserAction($id)
    {
    	$user = $this->getDoctrine()->getRepository('GraceWebBundle:User')->find($id);
    	if (!$user) $this->createNotFoundException('User not found');
    	$this->get('security.context')->getToken()->setUser($user);
    	return $this->redirect($this->generateUrl('_admin_index'));
    }
    
}




