<?php

namespace GraceCom\WebsiteBundle\Controller;

use GraceCom\WebsiteBundle\Entity\FeatureVote;

use Doctrine\ORM\Mapping\OrderBy;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Response;
use GraceCom\WebsiteBundle\Form\RosterItemType;
use GraceCom\WebsiteBundle\Form\RosterItemPersonType;
use GraceCom\WebsiteBundle\Form\RosterItemTextType;
use GraceCom\WebsiteBundle\Entity\RosterItem;

/**
 * @Route("/ajax")
 */
class AjaxController extends Controller
{
    
	/**
	* @Route("/swaplistitem/{type}/{to}/{id}", name="_ajax_swap_listitem")
	*/
	public function swapListItem($type,$id,$to)
	{
		$item = $this->getDoctrine()->getRepository('GraceWebBundle:'.$type)->find($id);
		if (!$item) return new Response(json_encode(array('success'=>false,'error'=>'Item 1 not found')));
		
		$item2 = $this->getDoctrine()->getRepository('GraceWebBundle:'.$type)->find($to);
		if (!$item2) return new Response(json_encode(array('success'=>false,'error'=>'Item 2 not found')));
		
		$from_pos = $item->getListOrder();
		$to_pos = $item2->getListOrder();
		$item->setListOrder($to_pos);
		$item2->setListOrder($from_pos);
		
		$em = $this->getDoctrine()->getEntityManager();
		$em->persist($item);
		$em->persist($item2);
		$em->flush();
		
		$response = array(
			'success'=>'true',
			'position1'=>$to_pos,
			'position2'=>$from_pos
		);
		return new Response(json_encode($response));
	}
	
	/**
	 * @Route("/publish/{type}/{id}/{value}", name="_ajax_publish")
	 */
	public function publish($type,$id,$value)
	{
		try {
			$item = $this->getDoctrine()->getRepository('GraceWebBundle:'.$type)->find($id);
		} catch (\Exception $e) {
			return new Response(json_encode(array('success'=>false,'error'=>'Bad entity type: '.$type)));
		}
		
		if (!$item) return new Response(json_encode(array('success'=>false,'error'=>'Item 1 ('.$id.') not found')));
	
		try {
			$item->setPublished($value);
		} catch (\Exception $e) {
			return new Response(json_encode(array('succes'=>false,'error'=>$e->getMessage())));
		}
	
		$em = $this->getDoctrine()->getEntityManager();
		$em->persist($item);
		$em->flush();
	
		$response = array(
				'success'=>true,
		);
		return new Response(json_encode($response));
	}
    
	/**
	* @Route("/votefeature/{id}", name="_ajax_vote_feature")
    * @Secure(roles="ROLE_ADMIN")
	*/
	public function voteFeature($id)
	{
		$feature = $this->getDoctrine()->getRepository('GraceWebBundle:Feature')->find($id);
		if (!$feature) return new Response(json_encode(array('success'=>false,'error'=>'No such feature')));
		if (!$feature->canVote($this->get('security.context')->getToken()->getUser())) 
		  return new Response(json_encode(array('success'=>false,'error'=>'Already voted for')));
		$vote = new FeatureVote();
		$vote->setFeature($feature);
		$vote->setUser($this->get('security.context')->getToken()->getUser());
		$vote->setTimestamp(new \DateTime());
		$feature->addFeatureVote($vote);
		$em = $this->getDoctrine()->getEntityManager();
		$em->persist($vote);
		
		$votes = count($feature->getVotes());
		
		$em->flush();
		
		$response = array(
			'success'=>'true',
			'votes'=>$votes,
		);
		return new Response(json_encode($response));
	}
    
    /**
     * @Route("/rosterwidget", name="_ajax_rosterwidget")
     */
    public function rosterWidget()
    {
    	   	
    	
    	$session = $this->getRequest()->getSession();
    	$service_id = $session->get('service');
    	$service_id = 1;
    	$year = date('Y');
    	//die('service_id='.$service_id);
    	
    	if (null === $session->get('idNextSunday-'.$service_id))
    	{
    		
	    	// Figure out the right date.
	    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
	    	$qb->select('l')
	    		->add('from','GraceWebBundle:RosterLine l')
	    	    ->where(
	    			$qb->expr()->andx(
	    				$qb->expr()->eq('l.service','?1'),
	    				$qb->expr()->gt('l.date','?2')
	    			)
	    		)
	    		->orderBy('l.date','ASC')
	    		->setParameter(1,$service_id)
	    		->setParameter(2,date('Y-m-d'))
	    		->setMaxResults(1);
	    	$q = $qb->getQuery();
	    	$nextrosteritem = $q->getSingleResult();
	    	
	    	$session->set('dateNextSunday',$nextrosteritem->getDate());
	    	$session->set('idNextSunday-'.$service_id,$nextrosteritem->getId());
    	}
    	
    	$line_id = $session->get('idNextSunday-'.$service_id);
    	
    	
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('i','r','p')
    		->add('from','GraceWebBundle:RosterItem i')
    		->leftJoin('i.person','p')
    		->leftJoin('i.rosterrole','r')
    		->add('where','i.rosterline = ?1')
    		->orderBy('r.list_order','ASC');
    	$q = $qb->getQuery();
    	$q->setParameter(1,$line_id);
    	$items = $q->getResult();
    	
    	return new Response(array(
    		'responseCode'=>200,
    		'items'=>$items,
    	)); 		
    } 
    
	/**
	* @Route("/addrosteritem/{rosterRole}/{rosterLine}", name="_ajax_addrosteritem")
    * @Template()
	*/
    public function addRosterItemAction($rosterRole,$rosterLine)
    {
    	$rosterItem = new rosterItem();
    	$request = $this->get('request');
    	
    	$roleobj = $this->getDoctrine()->getRepository('GraceWebBundle:RosterRole')->find($rosterRole);
    	$lineobj = $this->getDoctrine()->getRepository('GraceWebBundle:RosterLine')->find($rosterLine);
    	$rosterItem->setRosterrole($roleobj);
    	$rosterItem->setRosterline($lineobj);
    	
	    if ($roleobj->getType()=='text') $form = $this->get('form.factory')->create(new rosterItemTextType(),$rosterItem);
    	else { 
			$form = $this->createFormBuilder($rosterItem)
			->add('person', 'entity', array(
					'class' => 'GraceWebBundle:Person',
					'label'=>$rosterItem->getRosterrole()->getName(),
					'query_builder' => function($repository) {
					return $repository->createQueryBuilder('p')->orderBy('p.first_name, p.last_name', 'ASC');},
					'required'=>false
			))->getForm();
		}
		
		if ('POST' == $request->getMethod()) {
			$form->bindRequest($request);
			if ($form->isValid()) {
				$rosterItem = $form->getData();
				$em = $this->getDoctrine()->getEntityManager();
				$em->persist($rosterItem);
				$em->flush();
		
				$this->get('session')->setFlash('notice', 'roster item Saved!');
		
				return $this->redirect($this->generateUrl('_admin_roster'));
			}
		}
    	    
    	return array('form' => $form->createView());
    }
    
	/**
	* @Route("/editrosteritem/{id}", name="_ajax_editrosteritem")
    * @Template()
	*/
	public function editRosterItemAction($id)
	{
		$rosterItem = $this->getDoctrine()->getRepository('GraceWebBundle:RosterItem')->find($id);
		if (!$rosterItem) $this->createNotFoundException('roster item not found');
		if ($rosterItem->getRosterrole()->getType()=='text') $form = $this->get('form.factory')->create(new rosterItemTextType(),$rosterItem);
		
		else { 
			$form = $this->createFormBuilder($rosterItem)
			->add('person', 'entity', array(
					'class' => 'GraceWebBundle:Person',
					'label'=>$rosterItem->getRosterrole()->getName(),
					'query_builder' => function($repository) {
					return $repository->createQueryBuilder('p')->orderBy('p.first_name, p.last_name', 'ASC');},
					'required'=>false
			))->getForm();
		}
		
		$request = $this->get('request');
	    	if ('POST' == $request->getMethod()) {
	    		$form->bindRequest($request);
	    		if ($form->isValid()) {
	    			$rosterItem = $form->getData();
	    			$em = $this->getDoctrine()->getEntityManager();
	    			$em->persist($rosterItem);
	    			$em->flush();
	    			 
	    			$this->get('session')->setFlash('notice', 'roster item Saved!');
	    			 
	    			return $this->redirect($this->generateUrl('_admin_roster'));
	    		}
	    	}
			
			return array('rosterItem'=>$rosterItem,'form'=>$form->createView());
		}
    
    /**
     * @Route("/deleterosteritem/{id}", name="_ajax_deleterosteritem")
     * @Template()
     */
    public function deleteRosterItemAction($id)
    {
    	$rosterItem = $this->getDoctrine()->getRepository('GraceWebBundle:RosterItem')->find($id);
    	if (!$rosterItem) $this->createNotFoundException('Roster item not found');
    	$form = $this->createFormBuilder($rosterItem)->getForm();
    	
    	return array('form'=>$form->createView());
    }
     
     
    /**
    * @Route("events", name="_ajax_events")
    * @Template()
    */
    public function eventsAction()
    {
    	$event = $this->getDoctrine()->getRepository('GraceWebBundle:Event')->findAll();
    	if (!$event) $this->createNotFoundException('Could not find '.$name.' event');
    	$page = $this->getPage('events');
    	$menu = $this->getMenu();
    	return array('events'=>$event);
    }
    
    
    /**
     * @Route("/rosterline/{line_id}", name="_ajax_rosterline")
     * @Template("")
     */
    public function rosterLineAction($line_id)
    {
    	 
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('l')
    	->add('from','GraceWebBundle:RosterLine l')
    	->add(
    			'where',
    			$qb->expr()->eq('l.id','?1')
    	   
    	);
    	$q = $qb->getQuery();
    	$q->setParameter(1,$line_id);
    	$q->setMaxResults(1);
    	$line = $q->getSingleResult();
    	 
    	return array(
    			'line'=>$line
    	);
    }
    
    
}
