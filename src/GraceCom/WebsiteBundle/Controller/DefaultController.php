<?php

namespace GraceCom\WebsiteBundle\Controller;

use Doctrine\ORM\Mapping\OrderBy;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     * @Route("home")
     * @Route("", name="_home")
     * @Template()
     */
    public function indexAction()
    {
    	
    	$request = $this->getRequest();
    	$session = $request->getSession();
    	$cookies = $request->cookies;
    	$service = $cookies->get('session');
    	$page = $this->getPage('home');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	$events = $this->getDoctrine()->getRepository('GraceWebBundle:Event')->findAllCurrent();
    	$audiofile = $this->getDoctrine()->getRepository('GraceWebBundle:AudioFile')->findLatest($service);
    	$article = $this->getDoctrine()->getRepository('GraceWebBundle:Article')->findLatest();
    	return array('service'=>$service, 'menu'=>$menu,'page'=>$page,'events'=>$events,'ministries'=>$ministries,
    		'audiofile'=>$audiofile,'article'=>$article,'resourceCategories'=>$resourceCategories,
    		'rosterWidgetData'=>$this->getRosterWidgetData(),'dateNextRosterDate'=>$session->get('dateNextRosterDate'));
    } 
    
    /**
     * @Route("roster", name="_roster")
     * @Template("")
     */
    public function rosterAction()
    {
    	
    	$page = $this->getPage('roster');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
    	
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('c','s')
    	->add('from','GraceWebBundle:Congregation c')
    	->leftJoin('c.services','s')
    	->orderBy('c.id','ASC');
    	$q = $qb->getQuery();
    	$congregations = $q->getResult();
    	
    	$request = $this->getRequest();
    	$session = $request->getSession();
    	$cookies = $request->cookies;
    	
    	$service_id = $session->get('service_id')>0 ? $session->get('service_id') : ($cookies->has('service_id') ? $cookies->get('service_id') : 1);
    	    	
    	return array(
    		'menu'=>$menu,
    		'ministries'=>$ministries,
    		'page'=>$page,
    		'congregations'=>$congregations,
    		'service_id'=>$service_id
    	); 	
    	
    	
    }   
    
    /**
     * @Route("rosterdetails", defaults={"service_id"=0,"year"=0,"term"=0}, name="_rosterde")
     * @Route("rosterdetails/{service_id}", defaults={"year"=0,"term"=0}, name="_rosterd")
    * @Route("rosterdetails/{service_id}/{year}/{term}", name="_rosterdetails")
    * @Route("rosterweek/{week}", name="_rosterweek")
    * @Template("")
    */
    public function rosterDetailsAction($service_id,$year,$term,$week=null)
    {
    	
    	$request = $this->getRequest();
    	$session = $request->getSession();	
    	 
    	if ($service_id == 0)
    		$service_id = $session->get('service_id')>0 ? $session->get('service_id') : ($cookies->has('service_id') ? $cookies->get('service_id') : 1);
    	if ($year == 0) 
    		$year = $session->get('year')>0 ? $session->get('year') : date('Y');
    	    	
    	if ($term == 0)
    		if ($session->get('term')) $term = $session->get('term');
    		else
	    	{    		 
	    		// Figure out the right term.
	    		$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
	    		$qb->select('l')
	    		->add('from','GraceWebBundle:RosterLine l')
	    		->where(
	    		$qb->expr()->andx(
	    		$qb->expr()->eq('l.published','1'),
	    		$qb->expr()->eq('l.service','?1'),
	    		$qb->expr()->gt('l.date','?2')
	    		)
	    		)
	    		->orderBy('l.date','ASC')
	    		->setParameter(1,$service_id)
	    		->setParameter(2,date('Y-m-d'))
	    		->setMaxResults(1);
	    		$q = $qb->getQuery();
	    		try {
	    			$nextrosteritem = $q->getSingleResult();
	    			if ($term == 0) $term = $nextrosteritem->getTerm();
	    		} catch(\Exception $e) {
	    			if ($term == 0) $term = 1;
	    		}
	    		
	    	}
    	if (!isset($date))
    		$date = new \DateTime();
    		if ($session->get('date')) $date = $session->get('date');
    		if (isset($nextrosteritem)) $date = $nextrosteritem->getDate();
    		else
    		{
	    		// Figure out the right term.
	    		$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
	    		$qb->select('l')
	    		->add('from','GraceWebBundle:RosterLine l')
	    		->where(
	    				$qb->expr()->andx(
	    						$qb->expr()->eq('l.published','1'),
	    						$qb->expr()->eq('l.service','?1'),
	    						$qb->expr()->gt('l.date','?2')
	    				)
	    		)
	    		->orderBy('l.date','ASC')
	    		->setParameter(1,$service_id)
	    		->setParameter(2,date('Y-m-d'))
	    		->setMaxResults(1);
	    		$q = $qb->getQuery();
	    		try {
	    			$nextrosteritem = $q->getSingleResult();
	    			$date = $nextrosteritem->getDate();
	    		} catch(\Exception $e) {
		    		$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
		    		$qb->select('l')
		    		->add('from','GraceWebBundle:RosterLine l')
		    		->where(
		    				$qb->expr()->andx(
		    						$qb->expr()->eq('l.service','?1')
		    				)
		    		)
		    		->orderBy('l.date','DESC')
		    		->setParameter(1,$service_id)
		    		->setMaxResults(1);
		    		$q = $qb->getQuery();
		    		try {
		    			$nextrosteritem = $q->getSingleResult();
		    			$date = $nextrosteritem->getDate();
		    		} catch(\Exception $e) {
		    			// not sure what to do here...
		    		}
	    		}
	    	}
    	
    	$session->set('service_id',$service_id);
    	$session->set('year',$year);
    	$session->set('term',$term);
    	$cookie = new Cookie('service_id', $service_id, (time() + 3600 * 24 * 7),$this->generateUrl('_roster'));
    	$response = new Response();
    	$response->headers->setCookie($cookie);
    	$response->send();
    	
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('r')
    		->add('from','GraceWebBundle:RosterRole r')
    		->where($qb->expr()->eq('r.service','?1'))
    		->orderBy('r.list_order','ASC')
    		->setParameter(1,$service_id);
    	$q = $qb->getQuery();
    	$roles = $q->getResult();
    	$role_ids = array(0);
    	foreach ($roles as $role) $role_ids[] = $role->getID();
    	
    	
    	$query = $this->getDoctrine()->getentitymanager()->createQuery('SELECT l, i, r from GraceWebBundle:RosterLine l left join l.rosteritems i, i.rosterroles r');
    	
    	$startmonth = 1 + (($term -1) * 3);
    	$endmonth = $startmonth + 3;
    	$endyear = $endmonth > 12 ? $year + 1 : $year;
    	$endmonth = $endmonth > 12 ? 1 : $endmonth;
    	
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('l','i','r','p')
    		->add('from','GraceWebBundle:RosterLine l')
    		->add('where','l.date > 2012-01-01')
    		->leftJoin('l.rosteritems','i')
    		->leftJoin('i.rosterrole','r')
    		->leftJoin('i.person','p')
    		->add(
    			'where',
    			$qb->expr()->andx(
	    			$qb->expr()->eq('l.published','1'),
	    			$qb->expr()->orx(
	    				$qb->expr()->in('r.id',$role_ids),
	    				'i.id is NULL'
	    			),
	    			$qb->expr()->between('l.date','?1','?2'),
			    	$qb->expr()->eq('l.term','?3'),
			    	$qb->expr()->eq('l.service','?4')
	    		)
    		)
    		->orderBy('l.date','ASC')
    		->add('orderBy','r.list_order','ASC');
    	$q = $qb->getQuery();
    	$q->setParameter(1,$year.'-01-01');
    	$q->setParameter(2,$year.'-12-31');
    	$q->setParameter(3,$term);
    	$q->setParameter(4,$service_id);
    	$lines = $q->getResult();
    	
    	return array(
    		'roles'=>$roles,
    		'lines'=>$lines,
    		'meta'=>array(
    			'year'=>$year,
    			'nextyear'=>$year+1,
    			'prevyear'=>$year-1,
    			'term'=>$term,
    			'date'=>$date,
    			'nextterm'=>($term+1>4?1:$term+1),
    			'nexttermyear'=>($term+1>4?$year+1:$year),
    			'prevterm'=>($term-1<1?4:$term-1),
    			'prevtermyear'=>($term-1<1?$year-1:$year),
    			'service'=>$service_id)
    	); 		
    }
    
    public function getRosterWidgetData()
    {
    	
    	
    	$request = $this->getRequest();
    	$session = $request->getSession();
    	$cookies = $request->cookies;
    	 
    	if ($session->get('service_id')>0)
    	{
    		$service_id = $session->get('service_id');
    	}
    	else
    	{
    		if ($cookies->has('service_id'))
    			$service_id = $cookies->get('service_id');
    		else $service_id = 1;
    	}
    	 
    	$cookie = new Cookie('service', $service_id, (time() + 3600 * 24 * 7),$this->generateUrl('_roster'));
    	$response = new Response();
    	$response->headers->setCookie($cookie);
    	$response->send();
    	
    	$year = date('Y');
    	//die('service_id='.$service_id);
    	 
    	if (null === $session->get('idNextRosterDate-'.$service_id))
    	{
    
    		// Figure out the right date.
    		$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    		$qb->select('l')
    			->add('from','GraceWebBundle:RosterLine l')
    			->where(
    				$qb->expr()->andx(
    					$qb->expr()->eq('l.service','?1'),
    					$qb->expr()->gt('l.date','?2')
    				)
    			)
    			->orderBy('l.date','ASC')
    			->setParameter(1,$service_id)
    			->setParameter(2,date('Y-m-d'))
    			->setMaxResults(1);
    		$q = $qb->getQuery();
    		try {
    			$nextrosteritem = $q->getSingleResult();
    			$session->set('dateNextRosterDate',$nextrosteritem->getDate());
    			$session->set('idNextRosterDate-'.$service_id,$nextrosteritem->getId());
    		} catch (\Exception $e) {
    			return false;
    		}
    
    	}
    	 
    	$line_id = $session->get('idNextRosterDate-'.$service_id);
    	     	 
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('i','r','p')
    		->add('from','GraceWebBundle:RosterItem i')
    		->leftJoin('i.person','p')
    		->leftJoin('i.rosterrole','r')
    		->add('where','i.rosterline = ?1')
    		->orderBy('r.list_order','ASC');
    	$q = $qb->getQuery();
    	$q->setParameter(1,$line_id);
    	$items = $q->getResult();
    	return $items;
    }
     
    /**
    * @Route("cells", name="_cells")
    * @Template()
    */
    public function cellsAction()
    {
    	$query = $this->getDoctrine()->getEntityManager()->createQueryBuilder();
    	$query->select('c')->add('from','GraceWebBundle:Cell c')->where('c.published = 1');
    	$request = $this->get('request');
    	$filter = array('gender'=>'','day'=>'','congregation'=>'');
    	if ('POST' == $request->getMethod()) {
    		$filter = array(); $parameters = array();
    		$filter['gender'] = $request->get('gender'); 
    		$filter['day'] = $request->get('day'); 
    		$filter['congregation'] = $request->get('congregation');
    		if ($filter['gender']) {
    			$query->andWhere('c.gender = :gender');
    			$query->setParameter('gender',$filter['gender']);
    		}
    		if ($filter['day']) {
    			$query->andWhere('c.day = :day');
    			$query->setParameter('day',$filter['day']);
    		}
    		if ($filter['congregation']) {
    			$query->andWhere('c.congregation = :congregation');
    			$query->setParameter('congregation',$filter['congregation']);
    		}
    	}
    	$days = $this->getDoctrine()->getRepository('GraceWebBundle:Cell')->findDays();
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder()
    	->select('c')->add('from','GraceWebBundle:Congregation c')
    	->where('c.published = 1')->orderBy('c.id','ASC');
    	$congregations = $qb->getQuery()->getResult();
    	$cells = $query->getQuery()->getResult();
    	$page = $this->getPage('cells');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	return array('menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'cells'=>$cells,'days'=>$days,'congregations'=>$congregations,'page'=>$page,'filter'=>$filter);
    }
     
    /**
    * @Route("{name}/{id}-cell", name="_cell")
    * @Template()
    */
    public function cellAction($name,$id)
    {
    	$cell = $this->getDoctrine()->getRepository('GraceWebBundle:Cell')->findOneById($id);
    	if (!$cell) throw $this->createNotFoundException('Could not find '.$name.' cell');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	return array('menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'cell'=>$cell);
    }
     
    /**
    * @Route("congregations", name="_congregations")
    * @Route("services", name="_services")
    * @Template()
    */
    public function congregationsAction()
    {
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder()
    	->select('c','s')->add('from','GraceWebBundle:Congregation c')
    	->leftJoin('c.services','s','WITH','s.published = 1')
    	->where('c.published = 1')->orderBy('c.id','ASC');
    	$congregations = $qb->getQuery()->getResult();
    	$page = $this->getPage('services');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	return array('menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'congregations'=>$congregations,'page'=>$page);
    }
     
    /**
    * @Route("Grace.{name}", name="_congregation")
    * @Template()
    */
    public function congregationAction($name)
    {
    	$name = str_replace('_',' ',urldecode($name));
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('c','s','p','e')->add('from','GraceWebBundle:Congregation c')
    	->leftJoin('c.services','s','WITH','s.published = 1')
    	->leftJoin('c.events','e','WITH','e.published = 1')
    	->leftJoin('c.profiles','p','WITH','p.published = 1')
    	->where($qb->expr()->andx('c.published = 1','c.shortName = ?1'));
    	$qb->setParameter(1,$name)->setMaxResults(1);
    	$congregation = $qb->getQuery()->getSingleResult();
    	if (!$congregation) throw $this->createNotFoundException('Could not find '.$name.' congregation');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	return array('menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'congregation'=>$congregation);
    }
     
    /**
    * @Route("iamnew", name="_iamnew")
    * @Template()
    */
    public function iamnewAction()
    {
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder()
    	->select('c')->add('from','GraceWebBundle:Congregation c')
    	->where('c.published = 1')->orderBy('c.id','ASC');
    	$congregations = $qb->getQuery()->getResult();
    	$page = $this->getPage('iamnew');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	if (!$congregations) $this->createNotFoundException('No congregations found');
    	return array('menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'congregations'=>$congregations,'page'=>$page);
    }
     
     
    /**
    * @Route("events", name="_events")
    * @Template()
    */
    public function eventsAction()
    {
    	$events = $this->getDoctrine()->getRepository('GraceWebBundle:Event')->findAllCurrent();
    	if (!$events) $this->createNotFoundException('No events found');
    	$page = $this->getPage('events');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	return array('menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'events'=>$events,'page'=>$page);
    }
     
    /**
    * @Route("{name}/{id}e", name="_event")
    * @Template()
    */
    public function eventAction($name,$id)
    {
    	$event = $this->getDoctrine()->getRepository('GraceWebBundle:Event')->findOneById($id);
    	if (!$event) throw $this->createNotFoundException('Could not find '.$name.' event');
    	$page = $this->getPage('events');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	return array('page'=>$page,'menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'event'=>$event);
    }
    
    /**
    * @Route("articles", name="_articles")
    * @Route("blog", name="_blog")
    * @Template()
    */
    public function articlesAction()
    {
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder()
    	->select('a')->add('from','GraceWebBundle:Article a')
    	->where('a.published = 1')->orderBy('a.date','DESC');
    	$articles = $qb->getQuery()->getResult();
    	$page = $this->getPage('articles');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	return array('menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'articles'=>$articles,'page'=>$page);
    }
     
    /**
     * @Route("articles/{id}_{title}", name="_article")
     * @Route("blog/{title}/{id}", name="_blogarticle")
     * @Template()
     */
    public function articleAction($title,$id)
    {
    	$article = $this->getDoctrine()->getRepository('GraceWebBundle:Article')->findOneById($id);
    	if (!$article) throw $this->createNotFoundException('Could not find '.$title.' article');
    	$page = $this->getPage('articles');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	return array('page'=>$page,'menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'article'=>$article);
    }
     
    /**
    * @Route("messages", name="_audiofiles")
    * @Route("messages/{name}/{service_id}", name="_audiofiles_byservice")
    * @Template()
    */
    public function audiofilesAction($service_id = NULL)
    {
    	$request = $this->getRequest();
    	$session = $request->getSession();
    	$cookies = $request->cookies;
    	if (!isset($service_id)) $service_id = $session->get('service_id')>0 ? $session->get('service_id') : ($cookies->has('service_id') ? $cookies->get('service_id') : 1);
    	$session->set('service_id',$service_id);
    	$cookie = new Cookie('service_id', $service_id, (time() + 3600 * 24 * 7),$this->generateUrl('_roster'));
    	$response = new Response();
    	$response->headers->setCookie($cookie);
    	$response->send();
    	if ($service_id) 
    	{
    		$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    		$qb->select('a')->add('from','GraceWebBundle:AudioFile a')
    		->where($qb->expr()->andx('a.published = 1','a.service=?1'))->orderBy('a.date','DESC');
    		$qb->setParameter(1,$service_id);
    		$audiofiles = $qb->getQuery()->getResult();
    	}
    	else
    	{ 
    		$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder()
    		->select('a')->add('from','GraceWebBundle:AudioFile a')
    		->where('a.published = 1')->orderBy('a.date','DESC');
    		$audiofiles = $qb->getQuery()->getResult();
    	}
    	
    	$page = $this->getPage('messages');
    	$menu = $this->getMenu();
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('c','s')
    	->add('from','GraceWebBundle:Congregation c')
    	->leftJoin('c.services','s')
    	->orderBy('c.id','ASC');
    	$q = $qb->getQuery();
    	$congregations = $q->getResult();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	return array('menu'=>$menu,'congregations'=>$congregations,'service_id'=>$service_id,'audiofiles'=>$audiofiles,'resourceCategories'=>$resourceCategories,'ministries'=>$ministries,'page'=>$page);
    }
    
    /**
     * @Route("resources", defaults={"name"=""}, name="_resources")
     * @Route("resources/{name}", name="_resourceCategory")
     * @Template()
     */
    public function resourcesAction($name)
    {
    	if ($name) {
	    	$name = str_replace('_',' ',urldecode($name));
	    	$category = $this->getDoctrine()->getRepository('GraceWebBundle:ResourceCategory')->findOneByName($name);
			$category_id = $category->getID();
		} else {
	    	$category = $this->getDoctrine()->getRepository('GraceWebBundle:ResourceCategory')->findOneByListOrder(1);
			$category_id = $category->getID();
		}
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder()
    		->select('r')->add('from','GraceWebBundle:ResourceFile r')
    		->where('r.published = 1 and r.category = '.$category_id)->orderBy('r.name','ASC');
    	$ResourceFiles = $qb->getQuery()->getResult();
    	$resourceCategories = $this->getDoctrine()->getRepository('GraceWebBundle:ResourceCategory')->findAllPublished();
    	$page = $this->getPage('resources'); $menu = $this->getMenu(); $ministries = $this->getMinistryMenu();
    	return array('menu'=>$menu,'ResourceFiles'=>$ResourceFiles,'resourceCategories'=>$resourceCategories,
    	'ministries'=>$ministries,'page'=>$page, 'cur_category'=>$category_id);
    }
     
    /**
    * @Route("ministries", name="_ministries")
    * @Template()
    */
    public function ministriesAction()
    {
    	$ministries = $this->getDoctrine()->getRepository('GraceWebBundle:Ministry')->findAllWithLeaders();
		$resourceCategories = $this->getResourceMenu();
    	$page = $this->getPage('ministries');
    	$menu = $this->getMenu();
    	return array('menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'page'=>$page);
    }
     
    /**
    * @Route("{name}/m", name="_ministry")
    * @Template()
    */
    public function ministryAction($name)
    {
    	$name = str_replace('_',' ',urldecode($name));
    	$ministry = $this->getDoctrine()->getRepository('GraceWebBundle:Ministry')->findOneByName($name);
    	if (!$ministry) throw $this->createNotFoundException('Could not find '.$name.' ministry');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	return array('menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'ministry'=>$ministry);
    }
       
    /**
     * @Route("location", name="_location")
     * @Template()
     */
    public function locationAction()
    {
    	$page = $this->getPage('location');	
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
		return array('menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'page'=>$page); 		
    } 
    
    /**
    * @Route("profiles", name="_profiles")
    * @Route("whoswho", name="_whoswho")
    * @Template()
    */
    public function profilesAction()
    {
    	$qb = $this->getDoctrine()->getentitymanager()->createQueryBuilder();
    	$qb->select('p')->add('from','GraceWebBundle:Profile p')->orderBy('p.list_order','ASC')->where('p.published = 1');
    	$profiles = $qb->getQuery()->getResult();
    	$page = $this->getPage('whoswho');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	return array('menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'profiles'=>$profiles,'page'=>$page);
    }
     
    /**
     * @Route("{name}/{id}p", name="_profile")
     * @Template()
     */
    public function profileAction($name,$id)
    {
    	$profile = $this->getDoctrine()->getRepository('GraceWebBundle:Profile')->findOneById($id);
    	if (!$profile) throw $this->createNotFoundException('Could not find '.$title.'\'s profile');
    	$page = $this->getPage('whoswho');
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	return array('page'=>$page,'menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'profile'=>$profile);
    }
       
    /**
     * @Route("{pageid}", name="_page")
     * @Template()
     */
    public function pageAction($pageid)
    {
    	$page = $this->getPage($pageid);
    	$menu = $this->getMenu();
    	$ministries = $this->getMinistryMenu();
		$resourceCategories = $this->getResourceMenu();
    	return array('menu'=>$menu,'ministries'=>$ministries,'resourceCategories'=>$resourceCategories,'page'=>$page);
    } 
    
    public function getPage($url)
    {
    	$query = $this->getDoctrine()->getRepository('GraceWebBundle:Page')->createQueryBuilder('p');
    	$query->select('p')
    	->where($query->expr()->andx('p.published = 1','p.url = ?1'))
    	->orderBy('p.list_order','ASC')
    	->setParameter(1,$url)
    	->setMaxResults(1);
    	try {
    		$page = $query->getQuery()->getSingleResult();
    		return $page;
    	} catch (\Exception $e)
    	{
    		throw $this->createNotFoundException('No page "'.$url.'" found. '.$e->getMessage());
    	}
    }
    
    public function getMenu()
    {
    	$repo = $this->getDoctrine()->getRepository('GraceWebBundle:Page');
    	$query = $repo->createQueryBuilder('p')
    		->select('p.url, p.title')
    		->where('p.published = 1')
    		->orderBy('p.list_order','ASC')
    		->getQuery();
    	$pages = $query->getResult();
    	 
    	if (!$pages)
    	throw $this->createNotFoundException('No pages found.');
    	 
    	return $pages;
    }
    
    public function getMinistryMenu()
    {
    	$repo = $this->getDoctrine()->getRepository('GraceWebBundle:Ministry');
    	$query = $repo->createQueryBuilder('m')
    		->select('m.name, m.id')
    		->where('m.published = 1')
    		->orderBy('m.list_order','ASC')
    		->getQuery();
    	$ministries = $query->getResult();
    	 
    	return $ministries;
    }
    
    public function getResourceMenu()
    {
    	return $this->getDoctrine()->getRepository('GraceWebBundle:ResourceCategory')->findAllPublished();
    }
    
    
}
