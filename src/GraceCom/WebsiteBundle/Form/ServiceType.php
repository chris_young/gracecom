<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ServiceType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('congregation', 'entity', array(
	    	'class' => 'GraceWebBundle:Congregation',
	    	'query_builder' => function($repository) { return $repository->createQueryBuilder('c')->orderBy('c.id', 'ASC'); },
	    	'property' => 'name',
		));
    	$builder->add('name');
        $builder->add('day');
        $builder->add('start_time','time');
        $builder->add('location');
        $builder->add('rosterFormat','choice',array(
        		'choices'=>array(
        				'horizontal'=>'Horizontal (default)',
        				'vertical'=>"Vertical"
        		)
        ));
    }
    
    public function getName()
    {
        return 'service';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\Service',
    	);
    }
}
