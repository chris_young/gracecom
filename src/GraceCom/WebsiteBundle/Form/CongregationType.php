<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class CongregationType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('name');
        $builder->add('shortName','text',array('label'=>'Short name'));
        $builder->add('initials');
        $builder->add('file','file',array('label'=>'Image (1mb max)','required'=>false));
        $builder->add('short_text','textarea');
        $builder->add('text','textarea');
    }
    
    public function getName()
    {
        return 'congregation';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\Congregation',
    	);
    }
}
