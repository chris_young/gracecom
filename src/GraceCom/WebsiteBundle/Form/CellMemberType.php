<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class CellMemberType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('person', 'entity', array(
        	    	'class' => 'GraceWebBundle:Person',
        	    	'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.first_name, p.last_name', 'ASC'); }
        ));
        $builder->add('role','choice',array('choices'=>array('member'=>'Member','leader'=>'Leader')));
    }
    
    public function getName()
    {
        return 'CellMember';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\CellMember',
    	);
    }
}
