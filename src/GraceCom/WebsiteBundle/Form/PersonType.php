<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class PersonType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('firstname');
        $builder->add('lastname');
        $builder->add('email');
        $builder->add('mobile');
        $builder->add('text', 'textarea', array('required'=>false,'label'=>'Notes'));
		$builder->add('congregation', 'entity', array(
	    	'class' => 'GraceWebBundle:Congregation',
	    	'query_builder' => function($repository) { return $repository->createQueryBuilder('c')->orderBy('c.id', 'ASC'); },
	    	'property' => 'name',
		));
    }
    
    public function getName()
    {
        return 'person';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\Person',
    	);
    }
}
