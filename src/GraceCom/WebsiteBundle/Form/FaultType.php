<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class FaultType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('subject');
        $builder->add('url');
        $builder->add('description','textarea');
        if ($options['data']) 
        {
        	$builder->add('status','choice',array(
        		'choices'=>array(
					'new'=>'New',
					'acknowledged'=>"Acknowledged",
					'resolved'=>"Resolved"        		
        		)
        	));
        }
    }
    
    public function getName()
    {
        return 'fault';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\Fault',
    	);
    }
}
