<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class AudioFileType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('date','date',array('widget'=>'single_text','format'=>'dd/MM/yyyy'));
        $builder->add('title');
        $builder->add('subtitle','text',array('required'=>false));
        $builder->add('speaker');
        $builder->add('description','textarea',array('required'=>false));
        $builder->add('length','time',array('widget'=>'text','with_seconds'=>true,'label'=>'length (h:m:s)'));
        $builder->add('filetmp','file',array('label'=>'File (max 20mb)','required'=>false));
		$builder->add('service', 'entity', array(
	    	'class' => 'GraceWebBundle:Service',
	    	'query_builder' => function($repository) { return $repository->createQueryBuilder('c')->orderBy('c.id', 'ASC'); },
		));
    }
    
    public function getName()
    {
        return 'AudioFile';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\AudioFile',
    	);
    }
}
