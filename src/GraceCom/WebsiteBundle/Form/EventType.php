<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class EventType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('name');
        $builder->add('date_start','date',array('widget'=>'single_text','format'=>'dd/MM/yyyy','required'=>true));
        $builder->add('date_end','date',array('widget'=>'single_text','format'=>'dd/MM/yyyy','required'=>false));
        $builder->add('time_start','time',array('widget'=>'text','required'=>false));
        $builder->add('time_end','time',array('widget'=>'text','required'=>false));
        $builder->add('location');
        $builder->add('file','file',array('label'=>'Image (1mb max)','required'=>false));
        $builder->add('short_text', 'textarea');
        $builder->add('text', 'textarea');
		$builder->add('congregation', 'entity', array(
	    	'class' => 'GraceWebBundle:Congregation',
	    	'query_builder' => function($repository) { return $repository->createQueryBuilder('c')->orderBy('c.id', 'ASC'); },
	    	'property' => 'name',
	    	'required'=>false,
	    	'empty_value'=>'All'
		));
    }
    
    public function getName()
    {
        return 'event';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\Event',
    	);
    }
}
