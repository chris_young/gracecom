<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class CellType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('name');
        $builder->add('time','time');
        $builder->add('day');
        $builder->add('location');
        $builder->add('contact');
        $builder->add('gender','choice',array('choices'=>array('Men'=>'Men','Women'=>'Women','Mixed'=>'Mixed','Couples'=>'Couples')));
        $builder->add('short_text', 'textarea');
        $builder->add('text', 'textarea');
		$builder->add('congregation', 'entity', array(
	    	'class' => 'GraceWebBundle:Congregation',
	    	'query_builder' => function($repository) { return $repository->createQueryBuilder('c')->orderBy('c.id', 'ASC'); },
	    	'property' => 'name',
	    	'required'=>false,
	    	'empty_value'=>'All/None'
		));
    }
    
    public function getName()
    {
        return 'cell';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\Cell',
    	);
    }
}
