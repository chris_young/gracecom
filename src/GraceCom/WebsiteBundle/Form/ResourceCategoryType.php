<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ResourceCategoryType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('name');
        $builder->add('description','textarea');
    }
    
    public function getName()
    {
        return 'resourceCategory';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\ResourceCategory',
    	);
    }
}
