<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ResourceFileType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('name');
        $builder->add('description','textarea',array('required'=>false));
        $builder->add('category', 'entity', array(
	    	'class' => 'GraceWebBundle:ResourceCategory',
	    	'query_builder' => function($repository) { return $repository->createQueryBuilder('c')->orderBy('c.id', 'ASC'); },
	    	'property' => 'name'
		));
       
        if ($options['data']->getId()) {
        	$builder->add('filetmp','file',array('label'=>'File (max 20mb)','required'=>false));
        } else {
        	$builder->add('filetmp','file',array('label'=>'File (max 20mb)','required'=>true));
        }
    }
    
    public function getName()
    {
        return 'ResourceFile';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\ResourceFile',
    	);
    }
}
