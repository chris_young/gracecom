<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class RosterroleType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
    	$builder->add('name');
        $builder->add('heading');
        $builder->add('type','choice',array('choices'=>array('person'=>'Person','text'=>'Free Text')));
    }
    
    public function getName()
    {
        return 'service';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\RosterRole',
    	);
    }
}
