<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class RosterItemType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('person', 'entity', array(
        	    	'class' => 'GraceWebBundle:Person',
        	    	'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.first_name, p.last_name', 'ASC'); },
        	    	'required'=>false
        ));
        $builder->add('freeText','text',array('required'=>false));
    }
    
    public function getName()
    {
        return 'rosterItem';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\RosterItem',
    	);
    }
}
