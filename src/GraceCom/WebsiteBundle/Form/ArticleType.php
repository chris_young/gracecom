<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('title');
        $builder->add('date','date',array('widget'=>'single_text','format'=>'dd/MM/yyyy'));
        $builder->add('file','file',array('label'=>'Image (1mb max)','required'=>false));
        if ($options['data']) $builder->add('noimage','checkbox',array('required'=>false));
        $builder->add('short_text', 'textarea');
        $builder->add('text', 'textarea');
		$builder->add('profile', 'entity', array(
	    	'class' => 'GraceWebBundle:Profile',
	    	'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.list_order', 'ASC'); },
	    	'property' => 'name',
	    	'required' => false,
			'empty_value' => 'None'
		));
    }
    
    public function getName()
    {
        return 'article';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\Article',
    	);
    }
}
