<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class pageType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
    	
    	$toolbar = array(
	    	array(
	    	        'name' => 'basicstyles',
	    	        'items' => array('Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat')
	    	),
	    	array(
	    	        'name' => 'paragraph',
	    	        'items' => array('NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote')
	    	),
	    	array(
	    	        'name' => 'links',
	    	        'items' => array('Link','Unlink')
	    	),
	    	    '/',
	    	array(
	    	        'name' => 'styles',
	    	        'items' => array('Format')
	    	),
	    	array(
	    	        'name' => 'clipboard',
	    	        'items' => array('Cut','Copy','PasteText','-','Undo','Redo')
	    	),
	    	array(
	    	        'name' => 'editing',
	    	        'items' => array('SpellChecker', 'Scayt')
	    	),
	    	array(
	    	        'name' => 'tools',
	    	        'items' => array('Maximize', 'ShowBlocks','-','About')
	    	),
    	);
    	
        $builder->add('title');
        $builder->add('text','ckeditor',array('toolbar'=>$toolbar));
        $builder->add('file','file',array('label'=>'Image (1mb max)','required'=>false));
        if ($options['data']) $builder->add('noimage','checkbox',array('required'=>false));
        else $builder->add('url');
    }
    
    public function getName()
    {
        return 'page';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\page',
    	);
    }
}
