<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class RosterItemTextType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('freeText');;
    }
    
    public function getName()
    {
        return 'service';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\RosterItem',
    	);
    }
}
