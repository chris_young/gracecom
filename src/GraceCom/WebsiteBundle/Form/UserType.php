<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class UserType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
    	$builder->add('name','text',array('label'=>'Actual name'));
    	$builder->add('username');
        $builder->add('password');
        $builder->add('email');
        $builder->add('role','choice',array('choices'=>array('ROLE_USER'=>'User','ROLE_ADMIN'=>'Admin','ROLE_SUPER_ADMIN'=>'Super Admin')));
        $builder->add('is_active','checkbox',array('required'=>false));
		$builder->add('congregation', 'entity', array(
	    	'class' => 'GraceWebBundle:Congregation',
	    	'query_builder' => function($repository) { return $repository->createQueryBuilder('c')->orderBy('c.id', 'ASC'); },
	    	'property' => 'name',
	    	'required' => false,
			'empty_value' => 'All'
		));
		$builder->add('profile', 'entity', array(
	    	'class' => 'GraceWebBundle:Profile',
	    	'query_builder' => function($repository) { return $repository->createQueryBuilder('p')->orderBy('p.list_order', 'ASC'); },
	    	'property' => 'name',
	    	'required' => false,
			'empty_value' => 'None'
		));
    }
    
    public function getName()
    {
        return 'user';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\User',
    	);
    }
}
