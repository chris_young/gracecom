<?php

namespace GraceCom\WebsiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class RosterLineType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('service', 'entity', array(
	    	'class' => 'GraceWebBundle:Service',
	    	'query_builder' => function($repository) { return $repository->createQueryBuilder('s')->orderBy('s.name', 'ASC'); },
	    	'property' => 'name',
		));
        $builder->add('date','date');
        $builder->add('excursion','text',array('required'=>false));
		$builder->add('rosteritems','collection',array('type'=>new RosterItemType(),
        'allow_add' => false,
        'by_reference' => false,));
    }
    
    public function getName()
    {
        return 'rosterLine';
    }
    
    public function getDefaultOptions(array $options)
    {
    	return array(
            'data_class' => 'GraceCom\WebSiteBundle\Entity\RosterLine',
    	);
    }
}
