<?php

namespace GraceCom\WebsiteBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\ORM\Mapping as ORM;
use \Serializable;

/**
 * GraceCom\WebsiteBundle\Entity\User
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class User implements Serializable, AdvancedUserInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $username
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string $name
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @ORM\Column(name="salt", type="string", length=40)
     */
    private $salt;

    /**
     * @ORM\Column(name="password", type="string", length=40)
     */
    private $password;

    /**
     * @var string $role
     * @ORM\Column(name="role", type="string", length=40)
     */
    private $role;
    
    /**
     * @ORM\Column(name="email", type="string", length=60, unique=true)
     */
    private $email;

    /**
    * @ORM\ManyToOne(targetEntity="Profile", inversedBy="User", fetch="LAZY")
    * @ORM\JoinColumn(name="profile_id", referencedColumnName="id", nullable="true")
    */
    private $profile;

    /**
    * @ORM\ManyToOne(targetEntity="Congregation", inversedBy="User", fetch="LAZY", cascade={"all"})
    * @ORM\JoinColumn(name="congregation_id", referencedColumnName="id", nullable="true")
    */
    private $congregation;

    /**
     * @var integer $facebook_id
     *
     * @ORM\Column(name="facebook_id", type="integer", nullable="true")
     */
    private $facebook_id;
  
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;
    
    /**
    * @ORM\OneToMany(targetEntity="Fault", mappedBy="creator", fetch="LAZY")
     */
    protected $faults;
    
    /**
    * @ORM\OneToMany(targetEntity="Feature", mappedBy="creator", fetch="LAZY")
     */
    protected $features;
    
    public function __construct()
    {  	
        $this->isActive = true;
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->faults = new \Doctrine\Common\Collections\ArrayCollection();
    	$this->features = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function getRoles()
    {
        return array($this->role);
    }

    public function equals(UserInterface $user)
    {
        return $user->getUsername() === $this->username;
    }

    public function eraseCredentials()
    {
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set facebook_id
     *
     * @param integer $facebookId
     */
    public function setFacebookId($facebookId)
    {
        $this->facebook_id = $facebookId;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get facebook_id
     *
     * @return integer 
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * Set congregation
     *
     * @param GraceCom\WebsiteBundle\Entity\Congregation $congregation
     */
    public function setCongregation($congregation)
    {
        if ($congregation instanceof \GraceCom\WebsiteBundle\Entity\Congregation)
        {
            $this->congregation = $congregation;
        }
        elseif (is_null($congregation))
        {
            $this->congregation = $congregation;
        }
        else
        {
            $mess = "setCongregation method expects instance of Congregation as argument 1 !";
            throw new \InvalidArgumentException($mess);
        }
        
    }

    /**
     * Get congregation
     *
     * @return GraceCom\WebsiteBundle\Entity\Congregation 
     */
    public function getCongregation()
    {
        return $this->congregation;
    }

    /**
     * Set salt
     *
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
    
    public function isAccountNonExpired()
    {
    	return true;
    }
    
    public function isAccountNonLocked()
    {
    	return true;
    }
    
    public function isCredentialsNonExpired()
    {
    	return true;
    }
    
    public function isEnabled()
    {
    	return $this->isActive;
    }    
    
    public function serialize() {
        return serialize(
        	array(
        		'id'			=> $this->id,
        		'username'		=> $this->username,
        		'salt'			=> $this->salt,
        		'password'		=> $this->password,
        		'facebook_id'	=> $this->facebook_id,
        		'isActive'		=> $this->isActive,
        		'congregation'		=> is_object($this->congregation) ? $this->congregation->getId() : $this->congregation,
        	)
        );
    }
    public function unserialize($data) {
        $array = unserialize($data);
        $this->id 			= $array['id'];
        $this->username 	= $array['username'];
        $this->salt 		= $array['salt'];
        $this->password 	= $array['password'];
        $this->facebook_id 	= $array['facebook_id'];
        $this->isActive 	= $array['isActive'];
        $this->congregation = $array['congregation'];
    }
   

    /**
     * Set role
     *
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Add faults
     *
     * @param GraceCom\WebsiteBundle\Entity\Fault $faults
     */
    public function addFault(\GraceCom\WebsiteBundle\Entity\Fault $faults)
    {
        $this->faults[] = $faults;
    }

    /**
     * Get faults
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getFaults()
    {
        return $this->faults;
    }

    /**
     * Add features
     *
     * @param GraceCom\WebsiteBundle\Entity\Features $features
     */
    public function addFeatures(\GraceCom\WebsiteBundle\Entity\Feature $features)
    {
        $this->features[] = $features;
    }

    /**
     * Get features
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * Add features
     *
     * @param GraceCom\WebsiteBundle\Entity\Feature $features
     */
    public function addFeature(\GraceCom\WebsiteBundle\Entity\Feature $features)
    {
        $this->features[] = $features;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set profile
     *
     * @param GraceCom\WebsiteBundle\Entity\Profile $profile
     */
    public function setProfile($profile)
    {
        if ($profile instanceof \GraceCom\WebsiteBundle\Entity\Profile)
        {
            $this->profile = $profile;
        }
        elseif (is_null($profile))
        {
            $this->profile = null;
        }
        else
        {
            $mess = "setProfile method expects instance of Profile as argument 1 !";
            throw new \InvalidArgumentException($mess);
        }
    }

    /**
     * Get profile
     *
     * @return GraceCom\WebsiteBundle\Entity\Profile 
     */
    public function getProfile()
    {
        return $this->profile;
    }
}