<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GraceCom\WebsiteBundle\Entity\Page
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Page
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var text $text
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
    * @Assert\File(maxSize="1048576")
    */
    public $file;
    
    /**
     * For removing the imate
     * @var boolean $noimage
     */
    public $noimage;

    /**
     * @var integer $list_order
     *
     * @ORM\Column(name="list_order", type="integer")
     */
    private $list_order;

    /**
     * @var integer $published
     *
     * @ORM\Column(name="published", type="integer")
     */
    private $published = 0;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text
     *
     * @param text $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Get text
     *
     * @return text 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set order
     *
     * @param integer $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set image
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
    
    public function getAbsoluteImagePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebImagePath()
    {
        return null === $this->image ? null : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/bundles/graceweb/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'img/pages';
    }
    
    public function upload()
    {
    	if ($this->noimage == 1) {
    		$this->setImage('');
    		//also delete the file!
    		return;
    	}
    	
    	// the file property can be empty if the field is not required
    	if (null === $this->file) {
    		return;
    	}
    	// we use the original file name here but you should
    	// sanitize it at least to avoid any security issues
    
    	// move takes the target directory and then the target filename to move to
    	$this->file->move($this->getUploadRootDir(), $this->file->getClientOriginalName());
    
    	// set the path property to the filename where you'ved saved the file
    	$this->image = str_replace(' ','_',$this->file->getClientOriginalName());
    
    	// clean up the file property as you won't need it anymore
    	$this->file = null;
    }

    /**
     * Set list_order
     *
     * @param integer $listOrder
     */
    public function setListOrder($listOrder)
    {
        $this->list_order = $listOrder;
    }

    /**
     * Get list_order
     *
     * @return integer 
     */
    public function getListOrder()
    {
        return $this->list_order;
    }

    /**
     * Set published
     *
     * @param integer $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return integer 
     */
    public function getPublished()
    {
        return $this->published;
    }
}