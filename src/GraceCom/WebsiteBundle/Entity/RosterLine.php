<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GraceCom\WebsiteBundle\Entity\RosterLine
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GraceCom\WebsiteBundle\Entity\RosterLineRepository")
 */
class RosterLine
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var date $date
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var integer $term
     *
     * @ORM\Column(name="term", type="integer")
     */
    private $term;

    /**
     * @var string $excursion
     *
     * @ORM\Column(name="excursion", type="string", length=255, nullable=true)
     */
    private $excursion;

    /**
     * @var integer $published
     *
     * @ORM\Column(name="published", type="integer")
     */
    private $published = 0;
    
    /**
    * @ORM\ManyToOne(targetEntity="Service", inversedBy="rosterlines")
    * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
    */
    protected $service;
    
    /**
    * @ORM\OneToMany(targetEntity="RosterItem", mappedBy="rosterline", cascade={"persist", "remove"})
     */
    protected $rosteritems;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param date $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return date 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set excursion
     *
     * @param string $excursion
     */
    public function setExcursion($excursion)
    {
        $this->excursion = $excursion;
    }

    /**
     * Get excursion
     *
     * @return string 
     */
    public function getExcursion()
    {
        return $this->excursion;
    }
    public function __construct()
    {
        $this->rosteritems = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set service
     *
     * @param GraceCom\WebsiteBundle\Entity\Service $service
     */
    public function setService(\GraceCom\WebsiteBundle\Entity\Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get service
     *
     * @return GraceCom\WebsiteBundle\Entity\Service 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Add rosteritems
     *
     * @param GraceCom\WebsiteBundle\Entity\RosterItem $rosteritems
     */
    public function addRosterItem(\GraceCom\WebsiteBundle\Entity\RosterItem $rosteritems)
    {
        $this->rosteritems[] = $rosteritems;
    }

    /**
     * Get rosteritems
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getRosteritems()
    {
        return $this->rosteritems;
    }

    /**
     * Set rosteritems
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function setRosteritems(\Doctrine\Common\Collections\ArrayCollection $rosterItems)
    {
        $this->rosteritems = $rosterItems;
    }
    
    public function __toString()
    {
    	try {
    		return $this->getDate()->format('d/m/y');
    	}
    	catch (Exception $e) {
    		return "";
    	}
    	return $date;
    }

    /**
     * Set term
     *
     * @param integer $term
     */
    public function setTerm($term)
    {
        $this->term = $term;
    }

    /**
     * Get term
     *
     * @return integer 
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set published
     *
     * @param integer $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return integer 
     */
    public function getPublished()
    {
        return $this->published;
    }
}