<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GraceCom\WebsiteBundle\Entity\Feature
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GraceCom\WebsiteBundle\Entity\FeatureRepository")
 */
class Feature
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $subject
     *
     * @ORM\Column(name="subject", type="string", length=255)
     */
    private $subject;

    /**
     * @var string $status
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status = 'new';

    /**
     * @var text $description
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var datetime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;
    
    /**
    * @ORM\ManyToOne(targetEntity="User", inversedBy="Fault")
    * @ORM\JoinColumn(name="creator", referencedColumnName="id")
    */
    private $creator;
    
    /**
    * @ORM\OneToMany(targetEntity="FeatureComment", mappedBy="feature", cascade={"remove"})
     */
    protected $comments;
    
    /**
    * @ORM\OneToMany(targetEntity="FeatureVote", mappedBy="feature", cascade={"remove"})
     */
    protected $votes;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set timestamp
     *
     * @param datetime $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * Get timestamp
     *
     * @return datetime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set creator
     *
     * @param integer $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * Get creator
     *
     * @return integer 
     */
    public function getCreator()
    {
        return $this->creator;
    }
    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->votes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add comments
     *
     * @param GraceCom\WebsiteBundle\Entity\FeatureComment $comments
     */
    public function addFeatureComment(\GraceCom\WebsiteBundle\Entity\FeatureComment $comments)
    {
        $this->comments[] = $comments;
    }

    /**
     * Get comments
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set status
     *
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add votes
     *
     * @param GraceCom\WebsiteBundle\Entity\FeatureVote $votes
     */
    public function addFeatureVote(\GraceCom\WebsiteBundle\Entity\FeatureVote $votes)
    {
        $this->votes[] = $votes;
    }

    /**
     * Get votes
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getVotes()
    {
        return $this->votes;
    }
    
    
    /**
    * Get votes
     *
    * @return true or false
    */
    public function canVote($user)
    {
    	$votes = $this->votes;
    	foreach ($votes as $vote)
    	{
    		if ($vote->getUser()->equals($user))
    			return false;
    	}
    	return true;
    }
}