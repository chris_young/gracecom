<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GraceCom\WebsiteBundle\Entity\CellMember
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CellMember
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $role
     *
     * @ORM\Column(name="role", type="string", length=6)
     */
    private $role;

    /**
    * @ORM\ManyToOne(targetEntity="Cell", inversedBy="cellmember")
    * @ORM\JoinColumn(name="cell_id", referencedColumnName="id")
    */
    protected $cell;

    /**
    * @ORM\ManyToOne(targetEntity="Person", inversedBy="cellmember")
    * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
    */
    protected $person;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set cell
     *
     * @param GraceCom\WebsiteBundle\Entity\Cell $cell
     */
    public function setCell(\GraceCom\WebsiteBundle\Entity\Cell $cell)
    {
        $this->cell = $cell;
    }

    /**
     * Get cell
     *
     * @return GraceCom\WebsiteBundle\Entity\Cell 
     */
    public function getCell()
    {
        return $this->cell;
    }

    /**
     * Set person
     *
     * @param GraceCom\WebsiteBundle\Entity\Person $person
     */
    public function setPerson(\GraceCom\WebsiteBundle\Entity\Person $person)
    {
        $this->person = $person;
    }

    /**
     * Get person
     *
     * @return GraceCom\WebsiteBundle\Entity\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }
}