<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Query\AST\InstanceOfExpression;

use Doctrine\ORM\Mapping as ORM;

/**
 * GraceCom\WebsiteBundle\Entity\RosterItem
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GraceCom\WebsiteBundle\Entity\RosterItemRepository")
 */
class RosterItem
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $free_text
     *
     * @ORM\Column(name="free_text", type="string", length=255, nullable=true)
     */
    private $free_text;
    
    /**
    * @ORM\ManyToOne(targetEntity="RosterRole", inversedBy="rosteritems")
    * @ORM\JoinColumn(name="roster_role_id", referencedColumnName="id")
    */
    protected $rosterrole;
    
    /**
    * @ORM\ManyToOne(targetEntity="RosterLine", inversedBy="rosteritems")
    * @ORM\JoinColumn(name="roster_line_id", referencedColumnName="id")
    */
    protected $rosterline;
    
    /**
    * @ORM\ManyToOne(targetEntity="Person", inversedBy="rosteritems")
    * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
    */
    protected $person;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set free_text
     *
     * @param string $freeText
     */
    public function setFreeText($freeText)
    {
        $this->free_text = $freeText;
    }

    /**
     * Get free_text
     *
     * @return string 
     */
    public function getFreeText()
    {
        return $this->free_text;
    }

    /**
     * Set rosterrole
     *
     * @param GraceCom\WebsiteBundle\Entity\RosterRole $rosterrole
     */
    public function setRosterrole(\GraceCom\WebsiteBundle\Entity\RosterRole $rosterrole)
    {
        $this->rosterrole = $rosterrole;
    }

    /**
     * Get rosterrole
     *
     * @return GraceCom\WebsiteBundle\Entity\RosterRole 
     */
    public function getRosterrole()
    {
        return $this->rosterrole;
    }

    /**
     * Set rosterline
     *
     * @param GraceCom\WebsiteBundle\Entity\RosterLine $rosterline
     */
    public function setRosterline(\GraceCom\WebsiteBundle\Entity\RosterLine $rosterline)
    {
        $this->rosterline = $rosterline;
    }

    /**
     * Get rosterline
     *
     * @return GraceCom\WebsiteBundle\Entity\RosterLine 
     */
    public function getRosterline()
    {
        return $this->rosterline;
    }

    /**
     * Set person
     *
     * @param GraceCom\WebsiteBundle\Entity\Person $person
     */
    public function setPerson($person)
    {
    	if ($person instanceof \GraceCom\WebsiteBundle\Entity\Person)
    	{
    		$this->person = $person;
    	}
    	elseif (is_null($person))
    	{
    		$this->person = null;
    	}
    	else
    	{
    		$mess = "setPerson method expects instance of Person as argument 1 !";
    		throw new \InvalidArgumentException($mess);
    	}
    }

    /**
     * Get person
     *
     * @return GraceCom\WebsiteBundle\Entity\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }
}