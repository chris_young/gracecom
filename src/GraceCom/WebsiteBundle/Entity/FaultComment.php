<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GraceCom\WebsiteBundle\Entity\FaultComment
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GraceCom\WebsiteBundle\Entity\FaultCommentRepository")
 */
class FaultComment
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var text $comment
     *
     * @ORM\Column(name="comment", type="text")
     */
    private $comment;

    /**
     * @var datetime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    /**
    * @ORM\ManyToOne(targetEntity="User", inversedBy="Fault", fetch="LAZY")
    * @ORM\JoinColumn(name="creator", referencedColumnName="id")
    */
    private $creator;
    
    /**
    * @ORM\ManyToOne(targetEntity="Fault", inversedBy="FaultComment", fetch="LAZY")
    * @ORM\JoinColumn(name="fault", referencedColumnName="id")
    */
    private $fault;
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param text $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get comment
     *
     * @return text 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set timestamp
     *
     * @param datetime $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * Get timestamp
     *
     * @return datetime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set creator
     *
     * @param integer $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * Get creator
     *
     * @return integer 
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set fault
     *
     * @param GraceCom\WebsiteBundle\Entity\Fault $fault
     */
    public function setFault(\GraceCom\WebsiteBundle\Entity\Fault $fault)
    {
        $this->fault = $fault;
    }

    /**
     * Get fault
     *
     * @return GraceCom\WebsiteBundle\Entity\Fault 
     */
    public function getFault()
    {
        return $this->fault;
    }
}