<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * GraceCom\WebsiteBundle\Entity\ResourceFile
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GraceCom\WebsiteBundle\Entity\ResourceFileRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ResourceFile
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $file
     *
     * @ORM\Column(name="file", type="string", length=255)
     */
    private $file;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;
    
    /**
     * @var string $size
     *
     * @ORM\Column(name="size", type="string", length=255)
     */
    private $size;
    
    /**
    * @Assert\File(maxSize="30000000")
    */
    public $filetmp;

     /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $name;

      /**
     * @var text $description
     *
     * @ORM\Column(name="description", type="text",nullable="true")
     */
    private $description;

    /**
     * @var integer $published
     *
     * @ORM\Column(name="published", type="integer")
     */
    private $published = 0;

    /**
    * @ORM\ManyToOne(targetEntity="ResourceCategory", inversedBy="resouceFiles")
    * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
    */
    protected $category;
   
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set file
     *
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }
    
    public function getAbsoluteResourceFilePath()
    {
        return null === $this->file ? null : $this->getUploadRootDir().'/'.$this->file;
    }

    public function getWebAudioPath()
    {
        return null === $this->file ? null : $this->getUploadDir().'/'.$this->file;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/bundles/graceweb/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/file in the view.
        return 'files/resources';
    } 
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->filetmp) {
            // do whatever you want to generate a unique name
            $this->file = $this->filetmp->getClientOriginalName();
            $this->type = $this->filetmp->getMimeType();
            $this->size = $this->filetmp->getClientSize();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->filetmp) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->filetmp->move($this->getUploadRootDir(), $this->file);

        unset($this->filetmp);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsoluteResourceFilePath()) {
            unlink($file);
        }
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set size
     *
     * @param string $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * Get size
     *
     * @return string 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set published
     *
     * @param integer $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return integer 
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set category
     *
     * @param GraceCom\WebsiteBundle\Entity\ResourceCategory $category
     */
    public function setCategory(\GraceCom\WebsiteBundle\Entity\ResourceCategory $category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return GraceCom\WebsiteBundle\Entity\ResourceCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }
}