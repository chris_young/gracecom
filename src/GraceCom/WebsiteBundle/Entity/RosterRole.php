<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GraceCom\WebsiteBundle\Entity\RosterRole
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GraceCom\WebsiteBundle\Entity\RosterRoleRepository")
 */
class RosterRole
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $list_order
     *
     * @ORM\Column(name="list_order", type="integer")
     */
    private $list_order;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=6)
     */
    private $type;

    /**
     * @var string $heading
     *
     * @ORM\Column(name="heading", type="string", length=22)
     */
    private $heading;
    
    /**
    * @ORM\ManyToOne(targetEntity="Service", inversedBy="rosterroles")
    * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
    */
    protected $service;
    
    /**
    * @ORM\OneToMany(targetEntity="RosterItem", mappedBy="rosterrole", cascade={"remove"})
     */
    protected $rosteritems;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set list_order
     *
     * @param integer $listOrder
     */
    public function setListOrder($listOrder)
    {
        $this->list_order = $listOrder;
    }

    /**
     * Get list_order
     *
     * @return integer 
     */
    public function getListOrder()
    {
        return $this->list_order;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
    public function __construct()
    {
        $this->rosteritems = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set service
     *
     * @param GraceCom\WebsiteBundle\Entity\Service $service
     */
    public function setService(\GraceCom\WebsiteBundle\Entity\Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get service
     *
     * @return GraceCom\WebsiteBundle\Entity\Service 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Add rosteritems
     *
     * @param GraceCom\WebsiteBundle\Entity\RosterItem $rosteritems
     */
    public function addRosterItem(\GraceCom\WebsiteBundle\Entity\RosterItem $rosteritems)
    {
        $this->rosteritems[] = $rosteritems;
    }

    /**
     * Get rosteritems
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getRosteritems()
    {
        return $this->rosteritems;
    }

    /**
     * Set heading
     *
     * @param string $heading
     */
    public function setHeading($heading)
    {
        $this->heading = $heading;
    }

    /**
     * Get heading
     *
     * @return string 
     */
    public function getHeading()
    {
        return $this->heading;
    }
}