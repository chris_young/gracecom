<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GraceCom\WebsiteBundle\Entity\Person
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GraceCom\WebsiteBundle\Entity\PersonRepository")
 */
class Person
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $first_name
     * @Assert\NotBlank()
     * @ORM\Column(name="first_name", type="string", length=255)
     */
    private $first_name;

    /**
     * @var string $last_name
     * @Assert\NotBlank()
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    private $last_name;

    /**
     * @var string $email
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string $mobile
     *
     * @ORM\Column(name="mobile", type="string", length=14, nullable=true)
     */
    private $mobile;

    /**
     * @var text $text
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
    * @ORM\ManyToOne(targetEntity="Congregation", inversedBy="people")
    * @ORM\JoinColumn(name="congregation_id", referencedColumnName="id", nullable=true)
    */
    protected $congregation;
    
    /**
    * @ORM\OneToMany(targetEntity="RosterItem", mappedBy="person", cascade={"remove"})
     */
    protected $rosteritems;
    
    /**
    * @ORM\OneToMany(targetEntity="CellMember", mappedBy="person", cascade={"remove"})
     */
    protected $cellmemberships;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
    }

    /**
     * Get first_name
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set text
     *
     * @param text $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Get text
     *
     * @return text 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set congregation
     *
     * @param GraceCom\WebsiteBundle\Entity\Congregation $congregation
     */
    public function setCongregation(\GraceCom\WebsiteBundle\Entity\Congregation $congregation)
    {
        $this->congregation = $congregation;
    }

    /**
     * Get congregation
     *
     * @return GraceCom\WebsiteBundle\Entity\Congregation 
     */
    public function getCongregation()
    {
        return $this->congregation;
    }
    public function __construct()
    {
        $this->rosteritems = new \Doctrine\Common\Collections\ArrayCollection();
    	$this->cellmemberships = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add rosteritems
     *
     * @param GraceCom\WebsiteBundle\Entity\RosterItem $rosteritems
     */
    public function addRosterItem(\GraceCom\WebsiteBundle\Entity\RosterItem $rosteritems)
    {
        $this->rosteritems[] = $rosteritems;
    }

    /**
     * Get rosteritems
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getRosteritems()
    {
        return $this->rosteritems;
    }

    /**
     * Add cellmemberships
     *
     * @param GraceCom\WebsiteBundle\Entity\CellMember $cellmemberships
     */
    public function addCellMember(\GraceCom\WebsiteBundle\Entity\CellMember $cellmemberships)
    {
        $this->cellmemberships[] = $cellmemberships;
    }

    /**
     * Get cellmemberships
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCellmemberships()
    {
        return $this->cellmemberships;
    }
    
    public function __toString()
    {
    	return $this->getFirstName().' '.$this->getLastName();
    }
}