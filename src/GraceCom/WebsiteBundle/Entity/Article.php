<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * GraceCom\WebsiteBundle\Entity\Article
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GraceCom\WebsiteBundle\Entity\ArticleRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Article
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var date $date
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var text $short_text
     *
     * @ORM\Column(name="short_text", type="text")
     */
    private $shortText;

    /**
     * @var text $text
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var integer $published
     *
     * @ORM\Column(name="published", type="integer")
     */
    private $published = 0;
    
    /**
    * @Assert\File(maxSize="1048576")
    */
    public $file;
    
    /**
     * For removing the imate
     * @var boolean $noimage
     */
    public $noimage;

    /**
    * @ORM\ManyToOne(targetEntity="User", inversedBy="Article", fetch="LAZY")
    * @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
    */
    private $creator;

    /**
    * @ORM\ManyToOne(targetEntity="Profile", inversedBy="User", fetch="LAZY")
    * @ORM\JoinColumn(name="profile_id", referencedColumnName="id")
    */
    private $profile;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set date
     *
     * @param date $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return date 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set text
     *
     * @param text $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Get text
     *
     * @return text 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set image
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set shortText
     *
     * @param text $shortText
     */
    public function setShortText($shortText)
    {
        $this->shortText = $shortText;
    }

    /**
     * Get shortText
     *
     * @return text 
     */
    public function getShortText()
    {
        return $this->shortText;
    }
    
    public function getAbsoluteImagePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    public function getWebImagePath()
    {
        return null === $this->image ? null : $this->getUploadDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/bundles/graceweb/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'img/articles';
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
    	if ($this->noimage == 1) {
    		$this->removeUpload();
    		$this->setImage('');
    	}
        if (null !== $this->file) {
	       	if (null !== $this->image) {
	       		$this->removeUpload();
	       	}
            // do whatever you want to generate a unique name
            $this->image = uniqid().'-'.str_replace(' ','_',$this->file->getClientOriginalName());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsoluteImagePath()) {
        	if (!is_dir($file) && file_exists($file))
        	{
            	unlink($file);
            }
        }
    }
    

    /**
     * Set creator
     *
     * @param GraceCom\WebsiteBundle\Entity\User $creator
     */
    public function setCreator(\GraceCom\WebsiteBundle\Entity\User $creator)
    {
        $this->creator = $creator;
    }

    /**
     * Get creator
     *
     * @return GraceCom\WebsiteBundle\Entity\User 
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set profile
     *
     * @param GraceCom\WebsiteBundle\Entity\Profile $profile
     */
	public function setProfile($profile)
    {
        if ($profile instanceof \GraceCom\WebsiteBundle\Entity\Profile)
        {
            $this->profile = $profile;
        }
        elseif (is_null($profile))
        {
            $this->profile = null;
        }
        else
        {
            $mess = "setProfile method expects instance of Profile as argument 1 !";
            throw new \InvalidArgumentException($mess);
        }
    }

    /**
     * Get profile
     *
     * @return GraceCom\WebsiteBundle\Entity\Profile 
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set published
     *
     * @param integer $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return integer 
     */
    public function getPublished()
    {
        return $this->published;
    }
}