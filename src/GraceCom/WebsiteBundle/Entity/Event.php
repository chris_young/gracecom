<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * GraceCom\WebsiteBundle\Entity\Event
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GraceCom\WebsiteBundle\Entity\EventRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Event
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var date $date_start
     * @Assert\Date()
     * @Assert\NotBlank()
     * @ORM\Column(name="date_start", type="date")
     */
    private $date_start;

    /**
     * @var date $date_end
     * @Assert\Date()
     * @ORM\Column(name="date_end", type="date", nullable=true)
     */
    private $date_end;

    /**
     * @var time $time_start
     * @Assert\Time()
     * @ORM\Column(name="time_start", type="time", nullable=true)
     */
    private $time_start;

    /**
     * @var time $time_end
     * @Assert\Time()
     * @ORM\Column(name="time_end", type="time", nullable=true)
     */
    private $time_end;

    /**
     * @var string $location
     * @Assert\NotBlank()
     * @ORM\Column(name="location", type="string", length=255)
     */
    private $location;

    /**
     * @var integer $published
     *
     * @ORM\Column(name="published", type="integer")
     */
    private $published = 0;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
    * @Assert\File(maxSize="1048576")
    */
    public $file;

    /**
     * @var text $short_text
     * @Assert\NotBlank()
     * @ORM\Column(name="short_text", type="text")
     */
    private $short_text;

    /**
     * @var text $text
     * @Assert\NotBlank()
     * @ORM\Column(name="text", type="text")
     */
    private $text;
    
    /**
    * @ORM\ManyToOne(targetEntity="Congregation", inversedBy="events")
    * @ORM\JoinColumn(name="congregation_id", referencedColumnName="id")
    */
    protected $congregation;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set date_start
     *
     * @param date $dateStart
     */
    public function setDateStart($dateStart)
    {
        $this->date_start = $dateStart;
    }

    /**
     * Get date_start
     *
     * @return date 
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * Set date_end
     *
     * @param date $dateEnd
     */
    public function setDateEnd($dateEnd)
    {
        $this->date_end = $dateEnd;
    }

    /**
     * Get date_end
     *
     * @return date 
     */
    public function getDateEnd()
    {
        return $this->date_end;
    }

    /**
     * Set time_start
     *
     * @param time $timeStart
     */
    public function setTimeStart($timeStart)
    {
        $this->time_start = $timeStart;
    }

    /**
     * Get time_start
     *
     * @return time 
     */
    public function getTimeStart()
    {
        return $this->time_start;
    }

    /**
     * Set time_end
     *
     * @param time $timeEnd
     */
    public function setTimeEnd($timeEnd)
    {
        $this->time_end = $timeEnd;
    }

    /**
     * Get time_end
     *
     * @return time 
     */
    public function getTimeEnd()
    {
        return $this->time_end;
    }

    /**
     * Set image
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set text
     *
     * @param text $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Get text
     *
     * @return text 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set congregation
     *
     * @param $congregation
     */
    public function setCongregation($congregation)
    {
        $this->congregation = $congregation;
    }

    /**
     * Get congregation
     *
     * @return GraceCom\WebsiteBundle\Entity\Congregation 
     */
    public function getCongregation()
    {
        return $this->congregation;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set short_text
     *
     * @param text $shortText
     */
    public function setShortText($shortText)
    {
        $this->short_text = $shortText;
    }

    /**
     * Get short_text
     *
     * @return text 
     */
    public function getShortText()
    {
        return $this->short_text;
    }
    
    public function getAbsoluteImagePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    public function getWebImagePath()
    {
        return null === $this->image ? null : $this->getUploadDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/bundles/graceweb/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'img/events';
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
	       	if (null !== $this->image) {
	       		$this->removeUpload();
	       	}
            // do whatever you want to generate a unique name
            $this->image = uniqid().'-'.str_replace(' ','_',$this->file->getClientOriginalName());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsoluteImagePath()) {
        	if (!is_dir($file) && file_exists($file))
        	{
            	unlink($file);
            }
        }
    }

    /**
     * Set location
     *
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set published
     *
     * @param integer $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return integer 
     */
    public function getPublished()
    {
        return $this->published;
    }
}