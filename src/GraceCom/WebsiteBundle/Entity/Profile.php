<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * GraceCom\WebsiteBundle\Entity\Profile
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Profile
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string $role
     *
     * @ORM\Column(name="role", type="string", length=255)
     */
    private $role;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
    * @Assert\File(maxSize="1048576")
    */
    public $file;

    /**
     * @var text $short_text
     *
     * @ORM\Column(name="short_text", type="text")
     */
    private $short_text;

    /**
     * @var text $text
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var integer $published
     *
     * @ORM\Column(name="published", type="integer")
     */
    private $published = 0;

    /**
    * @ORM\ManyToOne(targetEntity="Congregation", inversedBy="profiles")
    * @ORM\JoinColumn(name="congregation", referencedColumnName="id")
    */
    protected $congregation;

    /**
     * @var integer $list_order
     *
     * @ORM\Column(name="list_order", type="integer")
     */
    private $list_order;
    
    /**
    * @ORM\OneToMany(targetEntity="Article", mappedBy="creator_id", fetch="LAZY")
     */
    protected $articles;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set short_text
     *
     * @param text $shortText
     */
    public function setShortText($shortText)
    {
        $this->short_text = $shortText;
    }

    /**
     * Get short_text
     *
     * @return text 
     */
    public function getShortText()
    {
        return $this->short_text;
    }

    /**
     * Set text
     *
     * @param text $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Get text
     *
     * @return text 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set congregation
     *
     * @param integer $congregation
     */
    public function setCongregation($congregation)
    {
        $this->congregation = $congregation;
    }

    /**
     * Get congregation
     *
     * @return integer 
     */
    public function getCongregation()
    {
        return $this->congregation;
    }
    
    public function getAbsoluteImagePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    public function getWebImagePath()
    {
        return null === $this->image ? null : $this->getUploadDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/bundles/graceweb/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'img/profiles';
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
	       	if (null !== $this->image) {
	       		$this->removeUpload();
	       	}
            // do whatever you want to generate a unique name
            $this->image = uniqid().'-'.$this->file->getClientOriginalName();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsoluteImagePath()) {
        	if (!is_dir($file) && file_exists($file))
        	{
            	unlink($file);
            }
        }
    }

    /**
     * Set list_order
     *
     * @param integer $listOrder
     */
    public function setListOrder($listOrder)
    {
        $this->list_order = $listOrder;
    }

    /**
     * Get list_order
     *
     * @return integer 
     */
    public function getListOrder()
    {
        return $this->list_order;
    }

    /**
     * Set role
     *
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }
    public function __construct()
    {
        $this->articles = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add articles
     *
     * @param GraceCom\WebsiteBundle\Entity\Article $articles
     */
    public function addArticle(\GraceCom\WebsiteBundle\Entity\Article $articles)
    {
        $this->articles[] = $articles;
    }

    /**
     * Get articles
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Set published
     *
     * @param integer $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return integer 
     */
    public function getPublished()
    {
        return $this->published;
    }
}