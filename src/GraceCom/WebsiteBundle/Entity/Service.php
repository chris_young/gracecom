<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GraceCom\WebsiteBundle\Entity\Service
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GraceCom\WebsiteBundle\Entity\ServiceRepository")
 */
class Service
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string $day
     *
     * @ORM\Column(name="day", type="string", length=9)
     */
    private $day;

    /**
     * @var time $start_time
     *
     * @ORM\Column(name="start_time", type="time")
     */
    private $start_time;

    /**
     * @var string $location
     *
     * @ORM\Column(name="location", type="string", length=255)
     */
    private $location;
    
    /**
     * @var string $rosterFormat
     *
     * @ORM\Column(name="rosterFormat", type="string", length=255)
     */
    private $rosterFormat = "horizontal";
    
    /**
     * @var integer $published
     *
     * @ORM\Column(name="published", type="integer")
     */
    private $published = 0;
    
    /**
    * @ORM\ManyToOne(targetEntity="Congregation", inversedBy="services")
    * @ORM\JoinColumn(name="congregation_id", referencedColumnName="id")
    */
    protected $congregation;
    
    /**
    * @ORM\OneToMany(targetEntity="RosterLine", mappedBy="service", cascade={"remove"})
     */
    protected $rosterlines;
    
    /**
    * @ORM\OneToMany(targetEntity="RosterRole", mappedBy="service", cascade={"remove"})
     */
    protected $rosterroles;
    
    /**
    * @ORM\OneToMany(targetEntity="AudioFile", mappedBy="service", cascade={"remove"})
     */
    protected $audiofiles;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set day
     *
     * @param string $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**
     * Get day
     *
     * @return string 
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set start_time
     *
     * @param time $startTime
     */
    public function setStartTime($startTime)
    {
        $this->start_time = $startTime;
    }

    /**
     * Get start_time
     *
     * @return time 
     */
    public function getStartTime()
    {
        return $this->start_time;
    }

    /**
     * Set location
     *
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }
    public function __construct()
    {
        $this->rosterlines = new \Doctrine\Common\Collections\ArrayCollection();
    $this->rosterroles = new \Doctrine\Common\Collections\ArrayCollection();
    $this->audiofiles = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set congregation
     *
     * @param GraceCom\WebsiteBundle\Entity\Congregation $congregation
     */
    public function setCongregation(\GraceCom\WebsiteBundle\Entity\Congregation $congregation)
    {
        $this->congregation = $congregation;
    }

    /**
     * Get congregation
     *
     * @return GraceCom\WebsiteBundle\Entity\Congregation 
     */
    public function getCongregation()
    {
        return $this->congregation;
    }

    /**
     * Add rosterlines
     *
     * @param GraceCom\WebsiteBundle\Entity\RosterLine $rosterlines
     */
    public function addRosterLine(\GraceCom\WebsiteBundle\Entity\RosterLine $rosterlines)
    {
        $this->rosterlines[] = $rosterlines;
    }

    /**
     * Get rosterlines
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getRosterlines()
    {
        return $this->rosterlines;
    }

    /**
     * Add rosterroles
     *
     * @param GraceCom\WebsiteBundle\Entity\RosterRole $rosterroles
     */
    public function addRosterRole(\GraceCom\WebsiteBundle\Entity\RosterRole $rosterroles)
    {
        $this->rosterroles[] = $rosterroles;
    }

    /**
     * Get rosterroles
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getRosterroles()
    {
        return $this->rosterroles;
    }

    /**
     * Add audiofiles
     *
     * @param GraceCom\WebsiteBundle\Entity\AudioFile $audiofiles
     */
    public function addAudioFile(\GraceCom\WebsiteBundle\Entity\AudioFile $audiofiles)
    {
        $this->audiofiles[] = $audiofiles;
    }

    /**
     * Get audiofiles
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getAudiofiles()
    {
        return $this->audiofiles;
    }
    
    public function __toString()
    {
    	return $this->name.' ('.$this->getCongregation()->getInitials().')';
    }

    /**
     * Set published
     *
     * @param integer $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return integer 
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set rosterFormat
     *
     * @param string $rosterFormat
     */
    public function setRosterFormat($rosterFormat)
    {
        $this->rosterFormat = $rosterFormat;
    }

    /**
     * Get rosterFormat
     *
     * @return string 
     */
    public function getRosterFormat()
    {
        return $this->rosterFormat;
    }
}