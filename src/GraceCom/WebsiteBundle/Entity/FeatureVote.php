<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GraceCom\WebsiteBundle\Entity\FeatureVote
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class FeatureVote
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
    * @ORM\ManyToOne(targetEntity="Feature", inversedBy="FeatureComment", fetch="LAZY")
    * @ORM\JoinColumn(name="feature_id", referencedColumnName="id")
    */
    private $feature;

    /**
    * @ORM\ManyToOne(targetEntity="User", inversedBy="Fault", fetch="LAZY")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
    */
    private $user;

    /**
     * @var datetime $timestamp
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timestamp
     *
     * @param datetime $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * Get timestamp
     *
     * @return datetime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set feature
     *
     * @param GraceCom\WebsiteBundle\Entity\Feature $feature
     */
    public function setFeature(\GraceCom\WebsiteBundle\Entity\Feature $feature)
    {
        $this->feature = $feature;
    }

    /**
     * Get feature
     *
     * @return GraceCom\WebsiteBundle\Entity\Feature 
     */
    public function getFeature()
    {
        return $this->feature;
    }

    /**
     * Set user
     *
     * @param GraceCom\WebsiteBundle\Entity\User $user
     */
    public function setUser(\GraceCom\WebsiteBundle\Entity\User $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return GraceCom\WebsiteBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}