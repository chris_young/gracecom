<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * GraceCom\WebsiteBundle\Entity\AudioFile
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GraceCom\WebsiteBundle\Entity\AudioFileRepository")
 * @ORM\HasLifecycleCallbacks
 */
class AudioFile
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $file
     *
     * @ORM\Column(name="file", type="string", length=255)
     */
    private $file;
    
    /**
    * @Assert\File(maxSize="20000000")
    */
    public $filetmp;

    /**
     * @var date $date
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string $subtitle
     *
     * @ORM\Column(name="subtitle", type="string", length=255, nullable="true")
     */
    private $subtitle;

    /**
     * @var string $speaker
     *
     * @ORM\Column(name="speaker", type="string", length=255)
     */
    private $speaker;

    /**
     * @var text $description
     *
     * @ORM\Column(name="description", type="text",nullable="true")
     */
    private $description;

    /**
     * @var time $length
     *
     * @ORM\Column(name="length", type="time")
     */
    private $length;

    /**
     * @var integer $published
     *
     * @ORM\Column(name="published", type="integer")
     */
    private $published = 0;
    
    /**
    * @ORM\ManyToOne(targetEntity="Service", inversedBy="audiofiles")
    * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
    */
    protected $service;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param date $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return date 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set speaker
     *
     * @param string $speaker
     */
    public function setSpeaker($speaker)
    {
        $this->speaker = $speaker;
    }

    /**
     * Get speaker
     *
     * @return string 
     */
    public function getSpeaker()
    {
        return $this->speaker;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set length
     *
     * @param time $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * Get length
     *
     * @return time 
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set service
     *
     * @param GraceCom\WebsiteBundle\Entity\Service $service
     */
    public function setService(\GraceCom\WebsiteBundle\Entity\Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get service
     *
     * @return GraceCom\WebsiteBundle\Entity\Service 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set file
     *
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }
    
    public function getAbsoluteAudioFilePath()
    {
        return null === $this->file ? null : $this->getUploadRootDir().'/'.$this->file;
    }

    public function getWebAudioPath()
    {
        return null === $this->file ? null : $this->getUploadDir().'/'.$this->file;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/bundles/graceweb/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/file in the view.
        return 'files';
    } 
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->filetmp) {
            // do whatever you want to generate a unique name
            $this->file = $this->filetmp->getClientOriginalName();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->filetmp) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->filetmp->move($this->getUploadRootDir(), $this->file);

        unset($this->filetmp);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsoluteAudioFilePath()) {
            unlink($file);
        }
    }

    /**
     * Set subtitle
     *
     * @param string $subtitle
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * Get subtitle
     *
     * @return string 
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set published
     *
     * @param integer $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return integer 
     */
    public function getPublished()
    {
        return $this->published;
    }
}