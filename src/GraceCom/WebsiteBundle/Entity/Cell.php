<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GraceCom\WebsiteBundle\Entity\Cell
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="GraceCom\WebsiteBundle\Entity\CellRepository")
 */
class Cell
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string $location
     *
     * @ORM\Column(name="location", type="string", length=255)
     */
    private $location;

    /**
     * @var string $day
     *
     * @ORM\Column(name="day", type="string", length=255)
     */
    private $day;

    /**
     * @var string $time
     *
     * @ORM\Column(name="time", type="time")
     */
    private $time;

    /**
     * @var string $gender
     *
     * @ORM\Column(name="gender", type="string", length=255)
     */
    private $gender;

    /**
     * @var string $contact
     *
     * @ORM\Column(name="contact", type="string", length=255)
     */
    private $contact;

    /**
     * @var text $short_text
     *
     * @ORM\Column(name="short_text", type="text")
     */
    private $short_text;

    /**
     * @var text $text
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var integer $list_order
     *
     * @ORM\Column(name="list_order", type="integer")
     */
    private $list_order;

    /**
     * @var integer $published
     *
     * @ORM\Column(name="published", type="integer")
     */
    private $published = 0;
    
    /**
    * @ORM\ManyToOne(targetEntity="Congregation", inversedBy="cells")
    * @ORM\JoinColumn(name="congregation_id", referencedColumnName="id")
    */
    protected $congregation;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set location
     *
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set picture
     *
     * @param string $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * Get picture
     *
     * @return string 
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set picture_modified
     *
     * @param datetime $pictureModified
     */
    public function setPictureModified($pictureModified)
    {
        $this->picture_modified = $pictureModified;
    }

    /**
     * Get picture_modified
     *
     * @return datetime 
     */
    public function getPictureModified()
    {
        return $this->picture_modified;
    }

    /**
     * Set text
     *
     * @param text $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Get text
     *
     * @return text 
     */
    public function getText()
    {
        return $this->text;
    }
    
    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set congregation
     *
     * @param $congregation
     */
    public function setCongregation($congregation)
    {
        $this->congregation = $congregation;
    }

    /**
     * Get congregation
     *
     * @return GraceCom\WebsiteBundle\Entity\Congregation 
     */
    public function getCongregation()
    {
        return $this->congregation;
    }

    /**
     * Set day
     *
     * @param string $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**
     * Get day
     *
     * @return string 
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set time
     *
     * @param time $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * Get time
     *
     * @return time 
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set short_text
     *
     * @param text $shortText
     */
    public function setShortText($shortText)
    {
        $this->short_text = $shortText;
    }

    /**
     * Get short_text
     *
     * @return text 
     */
    public function getShortText()
    {
        return $this->short_text;
    }

    /**
     * Set list_order
     *
     * @param integer $listOrder
     */
    public function setListOrder($listOrder)
    {
        $this->list_order = $listOrder;
    }

    /**
     * Get list_order
     *
     * @return integer 
     */
    public function getListOrder()
    {
        return $this->list_order;
    }

    /**
     * Set published
     *
     * @param integer $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return integer 
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set gender
     *
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set contact
     *
     * @param string $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }
}