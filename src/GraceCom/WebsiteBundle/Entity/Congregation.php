<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * GraceCom\WebsiteBundle\Entity\Congregation
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Congregation
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string $shortName
     *
     * @ORM\Column(name="short_name", type="string", length=10)
     */
    private $shortName;

    /**
     * @var string $initials
     *
     * @ORM\Column(name="initials", type="string", length=2)
     */
    private $initials;
    
    
    /**
    * @var string $short_text
     *
    * @ORM\Column(name="short_text", type="text")
    */
    private $short_text;
    
    
    /**
    * @var string $text
     *
    * @ORM\Column(name="text", type="text")
    */
    private $text;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var integer $published
     *
     * @ORM\Column(name="published", type="integer")
     */
    private $published = 0;
    
    /**
    * @Assert\File(maxSize="1048576")
    */
    public $file;
    
    
    /**
    * @ORM\OneToMany(targetEntity="Person", mappedBy="congregation")
    */
    protected $people;
    
    /**
     * @ORM\OneToMany(targetEntity="Service", mappedBy="congregation", cascade={"remove"})
     */
    protected $services;
    
    /**
    * @ORM\OneToMany(targetEntity="Profile", mappedBy="congregation")
    */
    protected $profiles;
    
    /**
    * @ORM\OneToMany(targetEntity="User", mappedBy="congregation")
     */
    protected $users;
    
    /**
    * @ORM\OneToMany(targetEntity="Event", mappedBy="congregation")
     */
    protected $events;
    
    /**
    * @ORM\OneToMany(targetEntity="Cell", mappedBy="congregation", cascade={"detatch"})
     */
    protected $cells;
    
    /**
    * @ORM\OneToMany(targetEntity="Ministry", mappedBy="congregation")
     */
    protected $ministries;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function __construct()
    {
        $this->people = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add people
     *
     * @param GraceCom\WebsiteBundle\Entity\Person $people
     */
    public function addPerson(\GraceCom\WebsiteBundle\Entity\Person $people)
    {
        $this->people[] = $people;
    }

    /**
     * Get people
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPeople()
    {
        return $this->people;
    }

    /**
     * Add services
     *
     * @param GraceCom\WebsiteBundle\Entity\Service $services
     */
    public function addService(\GraceCom\WebsiteBundle\Entity\Service $services)
    {
        $this->services[] = $services;
    }

    /**
     * Get services
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Add users
     *
     * @param GraceCom\WebsiteBundle\Entity\User $users
     */
    public function addUser(\GraceCom\WebsiteBundle\Entity\User $users)
    {
        $this->users[] = $users;
    }

    /**
     * Get users
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add events
     *
     * @param GraceCom\WebsiteBundle\Entity\Event $events
     */
    public function addEvent(\GraceCom\WebsiteBundle\Entity\Event $events)
    {
        $this->events[] = $events;
    }

    /**
     * Get events
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Add cells
     *
     * @param GraceCom\WebsiteBundle\Entity\Cell $cells
     */
    public function addCell(\GraceCom\WebsiteBundle\Entity\Cell $cells)
    {
        $this->cells[] = $cells;
    }

    /**
     * Get cells
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCells()
    {
        return $this->cells;
    }

    /**
     * Add ministries
     *
     * @param GraceCom\WebsiteBundle\Entity\Ministry $ministries
     */
    public function addMinistry(\GraceCom\WebsiteBundle\Entity\Ministry $ministries)
    {
        $this->ministries[] = $ministries;
    }

    /**
     * Get ministries
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getMinistries()
    {
        return $this->ministries;
    }

    /**
     * Set short_name
     *
     * @param string $shortName
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * Get short_name
     *
     * @return string 
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set initials
     *
     * @param string $initials
     */
    public function setInitials($initials)
    {
        $this->initials = $initials;
    }

    /**
     * Get initials
     *
     * @return string 
     */
    public function getInitials()
    {
        return $this->initials;
    }
    
    public function __toString()
    {
    	return $this->name;
    }

    /**
     * Set short_text
     *
     * @param text $shortText
     */
    public function setShortText($shortText)
    {
        $this->short_text = $shortText;
    }

    /**
     * Get short_text
     *
     * @return text 
     */
    public function getShortText()
    {
        return $this->short_text;
    }

    /**
     * Set text
     *
     * @param text $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Get text
     *
     * @return text 
     */
    public function getText()
    {
        return $this->text;
    }
    
    public function getAbsoluteImagePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    public function getWebImagePath()
    {
        return null === $this->image ? null : $this->getUploadDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/bundles/graceweb/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'img/congregations';
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
	       	if (null !== $this->image) {
	       		$this->removeUpload();
	       	}
            // do whatever you want to generate a unique name
            $this->image = uniqid().'-'.str_replace(' ','_',$this->file->getClientOriginalName());
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }
        

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->image);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsoluteImagePath()) {
        	if (!is_dir($file) && file_exists($file))
        	{
            	unlink($file);
            }
        }
    }

    /**
     * Set image
     *
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add profiles
     *
     * @param GraceCom\WebsiteBundle\Entity\Profile $profiles
     */
    public function addProfile(\GraceCom\WebsiteBundle\Entity\Profile $profiles)
    {
        $this->profiles[] = $profiles;
    }

    /**
     * Get profiles
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getProfiles()
    {
        return $this->profiles;
    }

    /**
     * Set published
     *
     * @param integer $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Get published
     *
     * @return integer 
     */
    public function getPublished()
    {
        return $this->published;
    }
}