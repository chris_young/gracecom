<?php

namespace GraceCom\WebsiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GraceCom\WebsiteBundle\Entity\MinistryMember
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MinistryMember
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $role
     *
     * @ORM\Column(name="role", type="string", length=6)
     */
    private $role;

    /**
    * @ORM\ManyToOne(targetEntity="Ministry", inversedBy="ministrymember")
    * @ORM\JoinColumn(name="ministry_id", referencedColumnName="id")
    */
    protected $ministry;

    /**
    * @ORM\ManyToOne(targetEntity="Person", inversedBy="ministrymember")
    * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
    */
    protected $person;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set ministry
     *
     * @param GraceCom\WebsiteBundle\Entity\Ministry $ministry
     */
    public function setMinistry(\GraceCom\WebsiteBundle\Entity\Ministry $ministry)
    {
        $this->ministry = $ministry;
    }

    /**
     * Get ministry
     *
     * @return GraceCom\WebsiteBundle\Entity\Ministry 
     */
    public function getMinistry()
    {
        return $this->ministry;
    }

    /**
     * Set person
     *
     * @param GraceCom\WebsiteBundle\Entity\Person $person
     */
    public function setPerson(\GraceCom\WebsiteBundle\Entity\Person $person)
    {
        $this->person = $person;
    }

    /**
     * Get person
     *
     * @return GraceCom\WebsiteBundle\Entity\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }
}