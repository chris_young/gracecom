$(document).ready(function(){
 $(".double-tabs #top-row li").delegate('a','click',function(event) {
  url = this.href;
  tab = url.substring(url.indexOf('#')+1);
  event.preventDefault();	
  $('.double-tabs div').hide();
  $('#'+tab).show();
  $(".double-tabs li").removeClass('active');
  $(this).parent().addClass('active');
  $('#'+tab+' a:eq(0)').click();
 })

 $(".double-tabs .bottom-row li").delegate('a','click',function(event) {
  $(".double-tabs .bottom-row li").removeClass('active');
  $(this).parent().addClass('active');
  if ($('div#roster').attr('id'))
	  $('div#roster').load(this.href) 
  else
      window.location = this.href;
  event.preventDefault();	
 });

 $(".double-tabs div").hide();
 $(".double-tabs div li.active").parent().parent().show();
 $(".double-tabs div.active").show();
});