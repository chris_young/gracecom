$(document).ready(function(){
  $('.orderable-list li #up').click(function(){
	  dir='up';
	  type=$(this).parent().attr('class');
	  list=$('.orderable-list li');
	  theitem=$(this).parent();
	  id=$(theitem).find('#id').html();
	  swapListItem(type,dir,list,theitem,id,swapurl);
  });
  $('.orderable-list li #down').click(function(){
	  dir='down';
	  type=$(this).parent().attr('class');
	  list=$('.orderable-list li');
	  theitem=$(this).parent();
	  id=$(theitem).find('#id').html();
	  swapListItem(type,dir,list,theitem,id,swapurl);
  });
  $('.admin-list #publish').click(function(){
	  type=$(this).parent().attr('class');
	  theitem=$(this).parent();
	  id=$(theitem).find('#id').html();
	  if ($(this).hasClass('on')) { if (publish(type,id,0)) $(this).removeClass('on').addClass('off'); } 
	  else if ($(this).hasClass('off')) { if (publish(type,id,1)) $(this).removeClass('off').addClass('on'); }
  });
});

/*
 * SwapListItem
 * swaps two items in a list using an ajax call. Needs the url because it is generated with symfony.
 */
  function swapListItem(type,dir,list,item,id,url)
  {
  	index = list.index(item);
  	if (dir=='up') newindex = index - 1;
  	else newindex = index + 1;
  	item2 = $(item).parent().find('li:eq('+newindex+')')
  	id2 = $(item2).find('#id').html();
  	
  	url = url.replace('%5Bid%5D',id);
  	url = url.replace('%5Bto%5D',id2);
  	url = url.replace('%5Btype%5D',type);
  	
  	jQuery.getJSON(url, function(response){
  	  if (response.success)
  	  {
  	  	if (dir=='up') item2.before(item.clone(true));
  	  	else item2.after(item.clone(true));
      	item.remove();
      }
      else alert(response.error)
  	});
  }
  
function publish(type,id,value)
{
  	url = publishurl;
  	url = url.replace('%5Bid%5D',id);
  	url = url.replace('%5Bvalue%5D',value);
  	url = url.replace('%5Btype%5D',type);

  	return jQuery.getJSON(url, function(response){
  	  if (response.success==true)
  	  {
  	  	return true;
      }
      else {
    	  alert('error: '+response.error);
    	  return false;
      }
  	});

}