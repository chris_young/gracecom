$(document).ready(function(){
 
    activeItem = $("#accordion li:first");
    $(activeItem).addClass('active');

    accordionwidth = 830; 
    accordionmargin = $("#accordion li").length * 10;
	accordionactivewidth = accordionwidth - accordionmargin;
	activeItem.css("width",accordionactivewidth);

    $("#accordion li").click(function(){
    	window.location = $(this).find('a.more').attr('href');
    });
    	
    $("#accordion li img.handle").click(function(event){
    	gotoAccordion($(this).parent());
		stopAccordion(aid);
		event.preventDefault();
    });
    
    $("#accordionkey li").click(function(){
    	gotoAccordion(this);
		stopAccordion(aid);
    });
    
    var aid = setInterval(nextAccordion,5000);
    
    $("#rosterwidget li").hide();
    $("#rosterwidget li:first").show();
    rosterwidgetactiveitem = $("#rosterwidget li:first");
    
    var rid = setInterval(function(){
    	nextid = $(rosterwidgetactiveitem).index()+2;
    	next = $("#rosterwidget li:nth-child("+nextid+")");
    	rosterwidgetactiveitem.hide();
    	next.show();
    	rosterwidgetactiveitem = next;
    },3000);
    
    
});

function nextAccordion(){
	index = $(activeItem).index() + 2;
	next = $("#accordion li:nth-child("+index+")");
	lastindex = $("#accordion li:last").index()+1;
	if (index > lastindex){
		first = $("#accordion li:first");
		gotoAccordion(first);
		
	} else {
		gotoAccordion(next);
	}
}

function gotoAccordion(hgyt){
	$(activeItem).animate({width: "10px"}, {duration:300, queue:false});
	index = $(hgyt).index() + 1;
    panel = $("#accordion li:nth-child("+index+")"); 
    key = $("#accordionkey li:nth-child("+index+")");
    $("accordionkey li").css("background-color","#999999");
    $(key).css("background-color","000000");
    $(panel).animate({width: accordionactivewidth}, {duration:300, queue:false});
	activeItem = $(panel);
}

function stopAccordion(aid) {
	clearInterval(aid);
}